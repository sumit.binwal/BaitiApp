//
//  AppDelegate.h
//  BaitiApp
//
//  Created by Shweta Rao on 24/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>
#import "IIViewDeckController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,IIViewDeckControllerDelegate>
{
  CLLocation *currentLocation;
}

@property (strong, nonatomic)IBOutlet UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) CLLocationManager             *locationManager;
@property (nonatomic, assign) CLLocationCoordinate2D currentLocationCoordinate;

//- (void)saveContext;
//- (NSURL *)applicationDocumentsDirectory;
- (IIViewDeckController*)generateControllerStack;
+(AppDelegate*) getSharedInstance;
@end

