//
//  LocationNameCollectionCell.h
//  BaitiApp
//
//  Created by Sumit Sharma on 28/05/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationNameCollectionCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblLocationName;

@end
