//
//  SalesFilterCustomTableCell.h
//  BaitiApp
//
//  Created by Shweta Rao on 20/05/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesFilterCustomTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblLocationName;

@end
