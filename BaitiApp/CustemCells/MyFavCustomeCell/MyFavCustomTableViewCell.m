//
//  MyFavCustomTableViewCell.m
//  BaitiApp
//
//  Created by Shweta Rao on 17/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "MyFavCustomTableViewCell.h"

@implementation MyFavCustomTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self=(MyFavCustomTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"MyFavCustomTableViewCell" owner:self options:nil] firstObject];
        self.frame=CGRectMake(0.0f, 0.0f,[[UIScreen mainScreen] bounds].size.width, 78.0f);
        // Initialization code
    }
    return self;
}

@end
