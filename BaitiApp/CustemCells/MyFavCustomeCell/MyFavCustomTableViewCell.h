//
//  MyFavCustomTableViewCell.h
//  BaitiApp
//
//  Created by Shweta Rao on 17/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "MGSwipeTableCell.h"
#import <QuartzCore/QuartzCore.h>
#import "MGSwipeTableCell.h"

@interface MyFavCustomTableViewCell : MGSwipeTableCell

@property (strong, nonatomic) IBOutlet UIImageView *imageForBuilderName;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UILabel *noOfRooms;
@property (strong, nonatomic) IBOutlet UILabel *lblForLocation;
@property (strong, nonatomic) IBOutlet UILabel *typeOfFlats;

@property (strong, nonatomic) IBOutlet UILabel *lblBed;
@property (strong, nonatomic) IBOutlet UILabel *lblBedNums;



@end
