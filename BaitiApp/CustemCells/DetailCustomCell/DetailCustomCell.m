//
//  DetailCustomCell.m
//  BaitiApp
//
//  Created by Shweta Rao on 20/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "DetailCustomCell.h"

@implementation DetailCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self=(DetailCustomCell *)[[[NSBundle mainBundle]loadNibNamed:@"DetailCustomCell" owner:self options:nil]objectAtIndex:0];
          self.frame=CGRectMake(0.0f, 0.0f,[[UIScreen mainScreen] bounds].size.width, 30.0f);
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
