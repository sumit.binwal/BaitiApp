//
//  CustomAgentListTableViewCell.h
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAgentListTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageForAgentName;
@property (strong, nonatomic) IBOutlet UILabel *lblForAgentName;
@property (strong, nonatomic) IBOutlet UILabel *lblForLocatioName;
@property (strong, nonatomic) IBOutlet UILabel *noOfPropertiesListed;

@property (strong, nonatomic) IBOutlet UILabel *lblPropertyListed;
@end
