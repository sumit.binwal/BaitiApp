//
//  CustomAgentListTableViewCell.m
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "CustomAgentListTableViewCell.h"

@implementation CustomAgentListTableViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self=(CustomAgentListTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"CustomAgentListTableViewCell" owner:self options:nil] firstObject];
        self.frame=CGRectMake(0.0f, 0.0f,[[UIScreen mainScreen] bounds].size.width, 72.0f);
        // Initialization code
        
        NSString *strProp;
        
        if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
        {
            strProp=[NSString stringWithFormat:@"الخصائص المذكورة" ];
        }
        else
        {
            strProp=@"Properties listed";
        }
        
        self.lblPropertyListed.text=strProp;

        
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
