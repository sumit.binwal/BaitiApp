//
//  MenuCustemCell.h
//  BaitiApp
//
//  Created by Shweta Rao on 04/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCustemCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *favouritImg;
@property (strong, nonatomic) IBOutlet UILabel *favouritLabl;
@property (strong, nonatomic) IBOutlet UILabel *favouritCountLbl;
@property (strong, nonatomic) IBOutlet UIImageView *favouritCountImg;

@end
