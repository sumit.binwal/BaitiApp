//
//  HomeTableViewCell.m
//  BaitiApp
//
//  Created by Shweta Rao on 25/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "HomeTableViewCell.h"

@implementation HomeTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self=(HomeTableViewCell *)[[[NSBundle mainBundle]loadNibNamed:@"HomeTableViewCell" owner:self options:nil]objectAtIndex:0];
        // Initialization code
    }
    return self;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
