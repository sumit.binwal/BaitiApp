//
//  NotificationsTableViewCell.m
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "NotificationsTableViewCell.h"

@implementation NotificationsTableViewCell

@synthesize btnAddFavourates;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self=(NotificationsTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"NotificationsTableViewCell" owner:self options:nil] firstObject];
        self.frame=CGRectMake(0.0f, 0.0f,[[UIScreen mainScreen] bounds].size.width, 60.0f);
              // Initialization code
        if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
        {
            [btnAddFavourates setTitle:@"إضافة المفضلة" forState:UIControlStateNormal];
        }
        else
        {
           [btnAddFavourates setTitle:@"add Favourites" forState:UIControlStateNormal];
        }

        
        
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
