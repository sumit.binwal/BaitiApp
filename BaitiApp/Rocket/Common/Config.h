//
//  Config.h
//  WhatzzApp
//
//  Created by Konstant on 22/05/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


#import <Foundation/Foundation.h>



@interface Config : NSObject {

}

//configuration section... 
extern  NSString		*SiteURL;
extern  NSString		*SiteAPIURL;

extern  NSString        *DatabaseName;
extern  NSString        *DatabasePath;
extern  NSString        *userName;
extern  NSString        *passWord;
extern  NSString        *privacy;
extern  NSString        *sid;
extern  NSString        *ringtonename;
extern  NSString        *pewPewPath;
extern  NSString        *taskdetles;
extern  NSString        *alarmdetles;
extern   NSString        *titlenotication;

extern  BOOL            isRegularUser;
extern  BOOL            canSwipe;
extern  BOOL            emptyRecord;
extern  float           CurrentLatitude;
extern  float           CurrentLongitude;
extern  int             totalPoints;
extern  int              localNotifIntervalTime;

extern NSDate           *repeatAlarm;


/**
 Device tocken for push notification
 */
extern NSString *userDeviceToken;
extern NSString *databaseName;
extern NSString *databasePath;
extern  NSString *language_id;
extern  NSMutableArray *filterDataArr;

@end
