//
//  TextFieldAlignment.h
//  RegistrationPage
//
//  Created by Shweta Rao on 11/02/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldAlignment : UITextField
- (CGRect)textRectForBounds:(CGRect)bounds;
- (CGRect)editingRectForBounds:(CGRect)bounds;
@end
