//
//  DetailVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 01/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "DetailVC.h"
#import "DetailCustomCell.h"
#import "DescriptionVC.h"
#import "AddNoteVC.h"
#import "SignInVC.h"
#import "EmailAgentVC.h"
#import "SDWebImageManager.h"
#import <CoreText/CTStringAttributes.h>
@interface DetailVC ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate>
{
    IBOutlet UIScrollView *scrllVwFullImage;
    
    IBOutlet NSLayoutConstraint *viewHeightContraint;
    IBOutlet NSLayoutConstraint *tblVwHeightContraint;
    IBOutlet UIView *viewFullImg;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UIButton *btnLocation;
    IBOutlet UITableView *detailTableVw;
    IBOutlet UILabel *lblProductDeatil;
    IBOutlet UIButton *btnPropertyType;
    IBOutlet UILabel *lblUserName;
    IBOutlet UIImageView *imgVwProfilePic;
    NSMutableArray *imagesArray;
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIScrollView *imgScrollVw;
    IBOutlet UILabel *lblImgCount;
    NSString *propertyID;
    NSString *propertyOwnerID;
    BOOL *fullImageViewAppear;//flag for fullimageview screen appear
    
    IBOutlet UIPageControl *fullImagePageControl;
}
@end

@implementation DetailVC
@synthesize detailDataDict;
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupNavigationBar];
    
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
    }
    else
    {
        isarabic=NO;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self setupNavigationBar];
    [self setUpView];
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
    }
    else
    {
        isarabic=NO;
    }
}
-(void)viewDidLayoutSubviews
{
    [scrllVw setScrollEnabled:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 568.0f)];
}
-(void)viewDidAppear:(BOOL)animated
{
    [self.view endEditing:YES];
}
#pragma mark-set NavigationBar

- (void)setUpView

{

    self.array=[[NSMutableArray alloc]initWithObjects:@"Price",@"Size",@"Number of rooms",@"Status", nil];
    
    
    [btnLocation setTitle:NSLocalizedString([[detailDataDict objectForKey:@"Property"]objectForKey:@"location"], nil) forState:UIControlStateNormal];
    lblProductDeatil.text=NSLocalizedString([[detailDataDict objectForKey:@"Property"]objectForKey:@"description"],nil);
    lblUserName.text=NSLocalizedString([[detailDataDict objectForKey:@"User"] objectForKey:@"username"],nil);
    [imgVwProfilePic setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[detailDataDict objectForKey:@"User"] objectForKey:@"image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSString *strpropertytype=[[detailDataDict objectForKey:@"Propertytype"]objectForKey:@"title"];

    [btnPropertyType setTitle:LOCALIZATION(strpropertytype)  forState: UIControlStateNormal];
    
    propertyOwnerID=[[detailDataDict objectForKey:@"Property"]objectForKey:@"user_id"];
    propertyID=[[detailDataDict objectForKey:@"Property"]objectForKey:@"id"];
    
    imagesArray=[detailDataDict objectForKey:@"PropertyImage"];
    
    if ([imagesArray count]>8) {
        
        pageControl.numberOfPages=8;
        fullImagePageControl.numberOfPages=8;
    }
    else
    {
        pageControl.numberOfPages=[imagesArray count];
                fullImagePageControl.numberOfPages=[imagesArray count];
    }
    DLog(@"images Array : --- %@",imagesArray);
    lblImgCount.text=[NSString stringWithFormat:@"1/%lu",(unsigned long)imagesArray.count];
    [self setUpForScrollView];
    
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [scrllVw setScrollEnabled:YES];
    
}
-(void)setupNavigationBar
{
    
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:[NSString stringWithFormat:@"%@",[[detailDataDict objectForKey:@"Property"] objectForKey:@"title"]]];
    if (isarabic)
    {
        [self.navigationItem setTitle:@"تفاصيل العقار"];
    }
    else
    {
        [self.navigationItem setTitle:@"Property Details"];
    }
    
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 28, 28)];
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
    UIEdgeInsets imageInsets;
    
    if([SYSTEM_VERSION floatValue]>=7.0f)
        imageInsets = UIEdgeInsetsMake(0.0f,-6.0f,0.0f, 00.0f);
    else
        imageInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    leftButton.imageEdgeInsets= imageInsets;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    UIButton *favRightBtn1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    if ([self isNotNull:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN]])
    {
        if([[[[NSUserDefaults standardUserDefaults] valueForKey:UD_FAVORITE] stringValue]isEqualToString:@"0"])
        {
            [favRightBtn1 setImage:[UIImage imageNamed:@"fav_icon"] forState:UIControlStateNormal];
        }
        else
        {
            [favRightBtn1 setImage:[UIImage imageNamed:@"favSelectedIcon"] forState:UIControlStateNormal];
        }
    }
    else
    {
        [favRightBtn1 setImage:[UIImage imageNamed:@"fav_icon"] forState:UIControlStateNormal];
    }
    
    [favRightBtn1 addTarget:self action:@selector(clickToFav:) forControlEvents:UIControlEventTouchUpInside];
    [favRightBtn1 setFrame:CGRectMake(18, 0, 32, 32)];
    
    UIButton *shareRightBtn2 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [shareRightBtn2 setImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];
    [shareRightBtn2 addTarget:self action:@selector(clickToshare:) forControlEvents:UIControlEventTouchUpInside];
    [shareRightBtn2 setFrame:CGRectMake(55, 0, 32, 32)];
    
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 76, 32)];
    [rightBarButtonItems addSubview:favRightBtn1];
    [rightBarButtonItems addSubview:shareRightBtn2];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    
}
-(void)extendLabelAccordingTotext
{
    float h = 0.0f;
    NSString *captionStr = @"dhweqfuhewuhfuwhefhuwehfuwehfuhweufhuwehfuwehfuewhfuewhufhweufhuewfhuwehfuewhfuhewufhwefuheuwhfdhweqfuhewuhfuwhefhuwehfuwehfuhweufhuwehfuwehfuewhfuewhufhweufhuewfhuwehfuewhfuhewufhwefuheuwhfdhweqfuhewuhfuwhefhuwehfuwehfuhweufhuwehfuwehfuewhfuewhufhweufhuewfhuwehfuewhfuhewufhwefuheuwhfdhweqfuhewuhfuwhefhuwehfuwehfuhweufhuwehfuwehfuewhfuewhufhweufhuewfhuwehfuewhfuhewufhwefuheuwhfdhweqfuhewuhfuwhefhuwehfuwehfuhweufhuwehfuwehfuewhfuewhufhweufhuewfhuwehfuewhfuhewufhwefuheuwhfdhweqfuhewuhfuwhefhuwehfuwehfuhweufhuwehfuwehfuewhfuewhufhweufhuewfhuwehfuewhfuhewufhwefuheuwhfdhweqfuhewuhfuwhefhuwehfuwehfuhweufhuwehfuwehfuewhfuewhufhweufhuewfhuwehfuewhfuhewufhwefuheuwhf";
    DLog(@"str %@",captionStr);
    lblProductDeatil.text = captionStr;
    
    
    static UIFont *fontUsed = nil;
    if (fontUsed == nil) fontUsed = [UIFont fontWithName:@"Oxygen Light" size:13.0f];
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          fontUsed, NSFontAttributeName,
                                          nil];
    
    CGRect frame = [captionStr boundingRectWithSize:CGSizeMake(300.0f, FLT_MAX)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:attributesDictionary
                                            context:nil];
    
    DLog(@"--------------------> %f",frame.size.height);
    
    if (frame.size.height>21.0f)
    {
        h = frame.size.height-21.0f ;
    }
    else
    {
        h = 21.0f;
    }
    DLog(@"height %f",h);
}

-(void)goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(void)clickToFav:(id)sender
{
    if([self isNotNull:[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN]])
    {
        if([CommonFunctions reachabiltyCheck])
        {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self favWebAPI];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
            
        }
    }
    else
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Baiti") message:LOCALIZATION(@"Please sign in")  delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alrt show];
    }
}
-(void)clickToshare:(id)sender
{
    NSString *texttoshare = [[detailDataDict objectForKey:@"Property"]objectForKey:@"title"];
    //    UIImage *imagetoshare;
    //    [imagetoshare s]
    NSArray *activityItems = @[texttoshare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint,UIActivityTypePostToFacebook,UIActivityTypePostToTwitter];
    [self presentViewController:activityVC animated:TRUE completion:nil];
    
}

#pragma mark-tableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return 3;

    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifair=@"DetailCustomCell";
    
    
    DetailCustomCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifair];
    
    if(cell==nil)
    {
        cell=[[DetailCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifair];
    }
    
    cell.labeLForStatus.text=LOCALIZATION([self.array objectAtIndex:indexPath.row]);
    
    if(indexPath.row==0)
    {
        cell.labelForMeasurements.text=[NSString stringWithFormat:@"%@ KWD",[[detailDataDict objectForKey:@"Property"] objectForKey:@"price"]];
    }
    else if (indexPath.row==1)
    {
        if([self isNotNull:[[detailDataDict objectForKey:@"Property"] objectForKey:@"size"]])
        {
            NSString *qlwUnitsPlainText = [NSString stringWithFormat:@"%@ m2",[[detailDataDict objectForKey:@"Property"] objectForKey:@"size"]];
            cell.labelForMeasurements.attributedText = [self plainStringToAttributedUnits: qlwUnitsPlainText];
        }
        else
        {
            cell.labelForMeasurements.text=@"";
        }
    }
    else if (indexPath.row==2)
    {
        cell.labelForMeasurements.text=[[detailDataDict objectForKey:@"Property"] objectForKey:@"no_of_bedroom"];
    }
    
    return cell;
}



#pragma mark- IBAction Buttons
- (IBAction)cameraIconClicked:(id)sender
{
    fullImageViewAppear=true;
    [viewFullImg setFrame:[UIScreen mainScreen].bounds];
    [self setUpForFullPageScrollView];
    self.navigationController.navigationBar.userInteractionEnabled=NO;
    [self.view addSubview:viewFullImg];
}
- (IBAction)goToDescriptionVC:(id)sender
{
    DescriptionVC *desc=ICLocalizedViewController([DescriptionVC class]);//[[DescriptionVC alloc]initWithNibName:@"DescriptionVC" bundle:nil];
    desc.dataDict=detailDataDict;
    [self.navigationController pushViewController:desc animated:YES];
}
- (IBAction)addNotesHere:(id)sender
{
    if([self isNotNull:[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN]])
    {
        AddNoteVC  *addNote=ICLocalizedViewController([AddNoteVC class]);//[[AddNoteVC alloc]initWithNibName:@"AddNoteVC" bundle:nil];
        addNote.propertyID=propertyID;
        [self.navigationController pushViewController:addNote animated:NO];
    }
    else
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Baiti") message:LOCALIZATION(@"Please sign in")  delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alrt show];
        
    }
}

- (IBAction)callAgentClicked:(id)sender
{
    
    NSString *phNo = [[detailDataDict objectForKey:@"Property"] objectForKey:@"phone"];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    DLog(@"%@",deviceType);
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]&&[deviceType isEqualToString:@"iPhone"])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:LOCALIZATION(@"Call facility is not available!!!") delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    
}
- (IBAction)emailAgent:(id)sender
{
    EmailAgentVC *email=ICLocalizedViewController([EmailAgentVC class]);//[[EmailAgentVC alloc]initWithNibName:@"EmailAgentVC" bundle:nil];
    email.dataDict=detailDataDict;
    email.strToEmail=@"fromDetail";
    [self.navigationController pushViewController:email animated:YES];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    if(buttonIndex==1)
    {
        SignInVC *sivc =ICLocalizedViewController([SignInVC class]);// [[SignInVC alloc]initWithNibName:@"SignInVC" bundle:nil];
        [(UINavigationController *)self.viewDeckController.centerController pushViewController:sivc animated:NO];
        [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:sivc]];
        [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
        }];
        
    }
    
}


-(void)setUpForScrollView
{
    
    for (int i = 0; i < [imagesArray count]; i++) {
        //We'll create an imageView object in every 'page' of our scrollView.
        CGRect frame;
        
        frame.origin.x = [UIScreen mainScreen].bounds.size.width * i;
        frame.origin.y = 0;
        frame.size = imgScrollVw.frame.size;
        frame.size.width=[UIScreen mainScreen].bounds.size.width;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
        //   imageView.backgroundColor=[UIColor redColor];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        CGFloat  imgViewHeight=imageView.frame.size.height;
        CGFloat  imgViewWidth=imageView.frame.size.width;
        
        //  [imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[imagesArray objectAtIndex:i] objectForKey:@"imagename"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        [imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[imagesArray objectAtIndex:i] objectForKey:@"imagename"]]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             imageView.image=[self imageByScalingAndCroppingForSize:CGSizeMake(imgViewWidth, imgViewHeight) WithImage:imageView.image];
             
         } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        
        [imgScrollVw addSubview:imageView];
    }
    //Set the content size of our scrollview according to the total width of our imageView objects.
    imgScrollVw.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width * [imagesArray count], imgScrollVw.frame.size.height);
}

- (void)gotoPage:(BOOL)animated
{
    NSInteger page = pageControl.currentPage;
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    // update the scroll view to the appropriate page
    CGRect bounds = imgScrollVw.bounds;
    bounds.origin.x = CGRectGetWidth(bounds) * page;
    bounds.origin.y = 0;
    [imgScrollVw scrollRectToVisible:bounds animated:animated];
}
- (void)loadScrollViewWithPage:(NSUInteger)page
{
    if (page >= imagesArray.count)
        return;
    
    //We'll create an imageView object in every 'page' of our scrollView.
    CGRect frame;
    
    frame.origin.x = [UIScreen mainScreen].bounds.size.width * page;
    frame.origin.y = 0;
    frame.size = imgScrollVw.frame.size;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    
    [imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[imagesArray objectAtIndex:page] objectForKey:@"imagename"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    [imgScrollVw addSubview:imageView];
    
}


- (IBAction)changePage:(id)sender
{
    [self gotoPage:YES];    // YES = animate
}


#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // Update the page when more than 50% of the previous/next page is visible
    if (fullImagePageControl) {
        CGFloat pageWidth = scrllVwFullImage.frame.size.width;
        int page = floor((scrllVwFullImage.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        fullImagePageControl.currentPage = page;

        
    }
    else
    {
        CGFloat pageWidth = imgScrollVw.frame.size.width;
        int page = floor((imgScrollVw.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        pageControl.currentPage = page;
        
        lblImgCount.text=[NSString stringWithFormat:@"%d/%lu",page+1,(unsigned long)imagesArray.count];
        
    }
}

#pragma mark- WebService API
-(void)favWebAPI
{
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/propertydetail
    NSString *siteURL = @"markasfavourite";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyID, @"propertyid",propertyOwnerID,@"property_owner_id",@"true",@"addremove",language_id,@"language_id",nil];
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN], @"token",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_fav_property"] forKey:UD_FAV_PROPERTY];
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_listed_property"] forKey:UD_LISTED_PROPERTY];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            
            if([[responseDict objectForKey:@"replyMsg"]isEqualToString:@"Property added successfully  in your favourite list."])
            {
                
                [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:UD_FAVORITE];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                UIButton *favRightBtn1 =  [UIButton buttonWithType:UIButtonTypeCustom];
                if([[[NSUserDefaults standardUserDefaults] objectForKey:UD_FAVORITE]isEqualToString:@"0"])
                {
                    [favRightBtn1 setImage:[UIImage imageNamed:@"fav_icon"] forState:UIControlStateNormal];
                }
                else
                {
                    [favRightBtn1 setImage:[UIImage imageNamed:@"favSelectedIcon"] forState:UIControlStateNormal];
                }
                
                [favRightBtn1 addTarget:self action:@selector(clickToFav:) forControlEvents:UIControlEventTouchUpInside];
                [favRightBtn1 setFrame:CGRectMake(18, 0, 32, 32)];
                
                UIButton *shareRightBtn2 =  [UIButton buttonWithType:UIButtonTypeCustom];
                [shareRightBtn2 setImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];
                [shareRightBtn2 addTarget:self action:@selector(clickToshare:) forControlEvents:UIControlEventTouchUpInside];
                [shareRightBtn2 setFrame:CGRectMake(55, 0, 32, 32)];
                
                UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 76, 32)];
                [rightBarButtonItems addSubview:favRightBtn1];
                [rightBarButtonItems addSubview:shareRightBtn2];
                
                self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
                
            }
            else
            {
                
                [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:UD_FAVORITE];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                UIButton *favRightBtn1 =  [UIButton buttonWithType:UIButtonTypeCustom];
                if([[[NSUserDefaults standardUserDefaults] objectForKey:UD_FAVORITE]isEqualToString:@"0"])
                {
                    [favRightBtn1 setImage:[UIImage imageNamed:@"fav_icon"] forState:UIControlStateNormal];
                }
                else
                {
                    [favRightBtn1 setImage:[UIImage imageNamed:@"favSelectedIcon"] forState:UIControlStateNormal];
                }
                
                [favRightBtn1 addTarget:self action:@selector(clickToFav:) forControlEvents:UIControlEventTouchUpInside];
                [favRightBtn1 setFrame:CGRectMake(18, 0, 32, 32)];
                
                UIButton *shareRightBtn2 =  [UIButton buttonWithType:UIButtonTypeCustom];
                [shareRightBtn2 setImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];
                [shareRightBtn2 addTarget:self action:@selector(clickToshare:) forControlEvents:UIControlEventTouchUpInside];
                [shareRightBtn2 setFrame:CGRectMake(55, 0, 32, 32)];
                
                UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 76, 32)];
                [rightBarButtonItems addSubview:favRightBtn1];
                [rightBarButtonItems addSubview:shareRightBtn2];
                
                self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
            }
        }
        else
        {
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              
                                              
                                              [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
                                          }
                                      }];
}

-(void)setUpForFullPageScrollView
{
    for (int i = 0; i < [imagesArray count]; i++) {
        //We'll create an imageView object in every 'page' of our scrollView.
        CGRect frame;
        frame.origin.x = [UIScreen mainScreen].bounds.size.width * i+20;
        frame.origin.y = 0;
        frame.size = scrllVwFullImage.frame.size;
        frame.size.width=[UIScreen mainScreen].bounds.size.width-30;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
      //  imageView.backgroundColor=[UIColor redColor];
        imageView.contentMode = UIViewContentModeScaleToFill;

        CGFloat  imgViewHeight=imageView.frame.size.height;
        CGFloat  imgViewWidth=imageView.frame.size.width;
        
        //[imageView setClipsToBounds:YES];
        
        [imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[imagesArray objectAtIndex:i] objectForKey:@"imagename"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
//        [imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[imagesArray objectAtIndex:i] objectForKey:@"imagename"]]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
//         {
//             imageView.image=[self imageByScalingAndCroppingForSize:CGSizeMake(imgViewWidth, imgViewHeight) WithImage:imageView.image];
//         } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [scrllVwFullImage addSubview:imageView];
    }
    //Set the content size of our scrollview according to the total width of our imageView objects.
    scrllVwFullImage.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width * [imagesArray count], scrllVwFullImage.frame.size.height);
    scrllVwFullImage.contentMode = UIViewContentModeRight;

}
#pragma mark- ScrollView setup For full Screen
- (void)gotofullPage:(BOOL)animated
{
    NSInteger page = pageControl.currentPage;
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    // update the scroll view to the appropriate page
    CGRect bounds = scrllVwFullImage.bounds;
    bounds.origin.x = CGRectGetWidth(bounds) * page;
    bounds.origin.y = 0;
    [scrllVwFullImage scrollRectToVisible:bounds animated:animated];
}
- (void)loadScrollViewWithfullPage:(NSUInteger)page
{
    if (page >= imagesArray.count)
        return;
    
    //We'll create an imageView object in every 'page' of our scrollView.
    CGRect frame;
    
    frame.origin.x = [UIScreen mainScreen].bounds.size.width * page;
    frame.origin.y = 0;
    frame.size = scrllVwFullImage.frame.size;
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
    
    [imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[imagesArray objectAtIndex:page] objectForKey:@"imagename"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [scrllVwFullImage addSubview:imageView];

}

#pragma mark -
#pragma mark Scale and crop image

- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize WithImage:(UIImage*)image
{
    UIImage *sourceImage = image;
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
        {
            scaleFactor = widthFactor; // scale to fit height
        }
        else
        {
            scaleFactor = heightFactor; // scale to fit width
        }
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight)*0.5 ;
        }
        else
        {
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth)*0.5;
            }
        }
    }
    
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    if(newImage == nil)
    {
        DLog(@"could not scale image");
    }
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;
}


- (IBAction)changefullPage:(id)sender
{
    [self gotoPage:YES];    // YES = animate
}


- (IBAction)imgcrossButtonClicked:(id)sender {
    [viewFullImg removeFromSuperview];
    fullImageViewAppear=false;
    self.navigationController.navigationBar.userInteractionEnabled=YES;
}

- (NSMutableAttributedString *)plainStringToAttributedUnits:(NSString *)string;
{
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    //Oxygen Bold 12.0
    UIFont *font = [UIFont fontWithName:@"Oxygen" size:12];//[UIFont systemFontOfSize:14.0f];
    UIFont *smallFont = [UIFont systemFontOfSize:9.0f];
    
    [attString beginEditing];
    [attString addAttribute:NSFontAttributeName value:(font) range:NSMakeRange(0, string.length - 2)];
    [attString addAttribute:NSFontAttributeName value:(smallFont) range:NSMakeRange(string.length - 1, 1)];
    [attString addAttribute:(NSString*)kCTSuperscriptAttributeName value:@"1" range:NSMakeRange(string.length - 1, 1)];
    [attString addAttribute:(NSString*)kCTForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, string.length - 1)];
    [attString endEditing];
    return attString;
}



@end
