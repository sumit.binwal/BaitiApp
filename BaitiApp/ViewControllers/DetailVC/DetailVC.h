//
//  DetailVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 01/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DetailVC : UIViewController
{

    IBOutlet UIButton *btnpropertyTitle;
    BOOL isarabic;
}

@property (strong, nonatomic) NSMutableDictionary *detailDataDict;



- (IBAction)cameraIconClicked:(id)sender;
- (IBAction)goToDescriptionVC:(id)sender;
- (IBAction)addNotesHere:(id)sender;
- (IBAction)callAgentClicked:(id)sender;
- (IBAction)emailAgent:(id)sender;


@property NSMutableArray * arrayOfImages;
@property NSMutableArray * array;
@property NSDictionary * dict;
@property UIImageView * imageView;


@end
