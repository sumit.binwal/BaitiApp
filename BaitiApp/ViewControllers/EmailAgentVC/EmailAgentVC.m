//
//  EmailAgentVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 30/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "EmailAgentVC.h"


@interface EmailAgentVC ()<UITextViewDelegate,UITextFieldDelegate>
{
    
    IBOutlet UIButton *btnSubmit;
    IBOutlet UIView *toolbarView;
    IBOutlet UILabel *lblMessage;
}
@end

@implementation EmailAgentVC
@synthesize dataDict,strToEmail,arrProperyData;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
    }
    else
    {
        isarabic=NO;
    }
    NSLog(@"Data Dict : -------%@",dataDict);
    
    //    self.scrollView.contentSize=_view.frame.size;
    //    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self textFieldPlaceholderClr];
    
    if ([strToEmail isEqualToString:@"fromDetail"])
    {
        
        _lblForLocation.text=[dataDict objectForKey:@"address"];
        if (_lblForLocation.text.length<3)
        {
            _lblForLocation.text=  [[dataDict objectForKey:@"User"]objectForKey:@"address"];
        }
        _lblForSendToName.text=[dataDict objectForKey:@"email"];
        if (_lblForSendToName.text.length<3)
        {
            _lblForSendToName.text=  [[dataDict objectForKey:@"Property"]objectForKey:@"email"];
        }
        
        NSData *data = [[dataDict objectForKey:@"username"] dataUsingEncoding:NSUTF8StringEncoding];
        _lblForPrice.text = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        if (_lblForPrice.text.length<3)
        {
            NSData *data = [[[dataDict objectForKey:@"User"]objectForKey:@"username"] dataUsingEncoding:NSUTF8StringEncoding];
            _lblForPrice.text = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
            
        }
        _lblForRooms.text=@"0";
        NSString *strImage=[dataDict objectForKey:@"image"];
        if (strImage.length<3)
        {
            strImage=  [[dataDict objectForKey:@"User"]objectForKey:@"image"];
        }
        [_imageForBuilderName setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[dataDict objectForKey:@"Property"]objectForKey:@"companylogo"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        
    }
    else
    {
        NSLog(@"%@",arrProperyData);
        _lblForPrice.text=[[arrProperyData objectAtIndex:0]objectForKey:@"username"];
        NSLog(@"%@",[dataDict objectForKey:@"username"]);
        _lblForLocation.text=[[arrProperyData objectAtIndex:0]objectForKey:@"address"];
        _lblForRooms.text=[dataDict objectForKey:@"total_properties"];
        _lblForSendToName.text=[[arrProperyData objectAtIndex:0]objectForKey:@"email"];
[_imageForBuilderName setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrProperyData objectAtIndex:0]objectForKey:@"image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
    }
    
    [_enterEmailHere setValue:[UIColor colorWithRed:102.0f/255.0f green:102.0f/255.0f blue:102.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [_enterName setValue:[UIColor colorWithRed:102.0f/255.0f green:102.0f/255.0f blue:102.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [_enterPhoneNum setValue:[UIColor colorWithRed:102.0f/255.0f green:102.0f/255.0f blue:102.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    DLog(@"%@",dataDict);
    _enterPhoneNum.inputAccessoryView=toolbarView;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setnavigationBar];
    [self setTitle];
    [self setButtonText];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setButtonText
{
    NSString *string0;
    NSString *string1;
    NSString *text;

        string0 =LOCALIZATION(@"Send") ;
        string1=LOCALIZATION(@"mail") ;
        
        text = [NSString stringWithFormat:@"%@ %@",string0,string1];
        
    
    
    
    //whole String attribute
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor colorWithRed:110.0f/255.0f green:110.0f/255.0f blue:110.0f/255.0f alpha:1],
                              NSFontAttributeName:[UIFont systemFontOfSize:14]
                              };
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text   attributes:attribs];
    
    NSRange string0Range = [text rangeOfString:string0];
    NSRange string1Range = [text rangeOfString:string1];
    
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Bold" size:18.0f]}  range:string0Range];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Light" size:18.0f]}  range:string1Range];
    
    [btnSubmit setAttributedTitle: attributedText forState:UIControlStateNormal];
    
}

#pragma mark - set NavigationBar
- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
    }
    

    titleView.text =LOCALIZATION(@"Email Agent") ;
     self.navigationItem.titleView = titleView;
    
    [titleView sizeToFit];
}
-(void)setnavigationBar
{
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 28, 28)];
    UIEdgeInsets imageInsets;
    
    if([SYSTEM_VERSION floatValue]>=7.0f)
        imageInsets = UIEdgeInsetsMake(0.0f,-6.0f,0.0f, 00.0f);
    else
        imageInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    leftButton.imageEdgeInsets= imageInsets;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
    UIButton * rightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 56, 22)];
    [rightButton addTarget:self action:@selector(sendMail:) forControlEvents:UIControlEventTouchUpInside];
    if (isarabic)
    {
        [rightButton setTitle:@"حفظ" forState:UIControlStateNormal];
    }
    else
    {
        [rightButton setTitle:@"Send" forState:UIControlStateNormal];
    }
    
    rightButton.titleLabel.font=[UIFont fontWithName:@"OXYGEN-REGULAR" size:12];
    [rightButton setBackgroundImage:[UIImage imageNamed:@"search_filter_applybut"] forState:UIControlStateNormal];
    rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    rightButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:rightButton];
    
}
-(void)goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)isValidate
{
           if (![CommonFunctions isValueNotEmpty:_enterName.text]) {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter name") ];
            return NO;
        }
        else if (![CommonFunctions isValueNotEmpty:_enterEmailHere.text])
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter Agent email") ];
            return NO;
        }
        else if (![CommonFunctions IsValidEmail:_enterEmailHere.text])
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter valid email") ];
            return NO;
        }
        else if (![CommonFunctions isValueNotEmpty:_enterPhoneNum.text])
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter phone number") ];
            return NO;
        }
        else if (_enterPhoneNum.text.length<10)
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter 10 digit phone number") ];
            return NO;
        }
        else if (![CommonFunctions isValueNotEmpty:_enterMessageHere.text])
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter message first") ];
            return NO;
        }
    
    
    return YES;
}

-(void)sendMail:(id)sender
{
    [self.view endEditing:YES];
    if ([self isValidate]) {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self sentMailToAgent];
        }
        else
        {
             [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
            
        }
    }
    
}
#pragma mark-textView Delegatemethod
-(void)textViewDidChange:(UITextView *)textView
{
    if(![textView hasText]) {
        lblMessage.hidden = NO;
    }
    else{
        lblMessage.hidden = YES;
    }
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    //    if ([self i]) {
    //        [lblMessage setHidden:YES];
    //    }
    //    else
    //    {
    //    [lblMessage setHidden:NO];
    //    }
    
    if ([text isEqualToString:@"\n"])
    {
        [self.view endEditing:YES];
    }
    
    return YES;
}

#pragma mark-textFiled place holder colour
-(void)textFieldPlaceholderClr
{
    [self.enterName setValue:[UIColor colorWithRed:116.0/255.0 green:116.0/255.0 blue:116.0/255.0 alpha:1.0]
                  forKeyPath:@"_placeholderLabel.textColor"];
    [self.enterPhoneNum setValue:[UIColor colorWithRed:116.0/255.0 green:116.0/255.0 blue:116.0/255.0 alpha:1.0]
                      forKeyPath:@"_placeholderLabel.textColor"];
    [self.enterEmailHere setValue:[UIColor colorWithRed:116.0/255.0 green:116.0/255.0 blue:116.0/255.0 alpha:1.0]
                       forKeyPath:@"_placeholderLabel.textColor"];
}




#pragma mark - textField Delegate Methods

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==_enterPhoneNum) {
        if (_enterPhoneNum.text.length>=12) {
            return NO;
        }

    }
    else if (textField==_enterName) {
        if (_enterName.text.length>=22) {
            return NO;
        }
        
    }
    else if (textField==_enterEmailHere) {
        if (_enterEmailHere.text.length>=30) {
            return NO;
        }
        
    }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==_enterName) {
        [self.enterName resignFirstResponder];
        [self.enterEmailHere becomeFirstResponder];
        
    }
    else if(textField==_enterEmailHere){
        [self.enterEmailHere resignFirstResponder];
        [self.enterPhoneNum becomeFirstResponder];
    }
    else if(textField==_enterPhoneNum)
    {
        [self.enterPhoneNum resignFirstResponder];
        [self.enterMessageHere becomeFirstResponder];
    }
    return YES;
}

#pragma mark- validate textFileds
-(BOOL)validateTextFields
{
    BOOL validate=NO;
    NSString * alertString;
    if ([_enterName.text length]==0) {
        textFld=_enterName;
        validate=NO;
        alertString=@"Please enter name";
        
    }
    else if ([_enterEmailHere.text length]==0) {
        textFld=_enterEmailHere;
        validate=NO;
        alertString=@"Please enter email";
        
    }
    else if ([_enterPhoneNum.text length]==0) {
        textFld=_enterPhoneNum;
        validate=NO;
        alertString=@"Please enter phone number";
        
    }
    if (alertString.length==0) {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"" message:LOCALIZATION(alertString)  delegate:self cancelButtonTitle:LOCALIZATION(@"OK" ) otherButtonTitles:nil, nil];
        [alert show];
    }
    return validate;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (textFld==_enterName)
    {
        [_enterName becomeFirstResponder];
    }
    else if (alertView.tag==100)
    {
        [self.navigationController popViewControllerAnimated:NO];
    }
    else if (textFld==_enterPhoneNum)
    {
        [_enterPhoneNum becomeFirstResponder];
    }
    else if (textFld==_enterEmailHere)
    {
        [_enterEmailHere becomeFirstResponder];
    }
    else
    {
        [textFld resignFirstResponder];
    }
    
}

#pragma mark-send Mail Action
- (IBAction)clickToSendMail:(id)sender
{
    [self.view endEditing:YES];
    if ([self isValidate]) {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self sentMailToAgent];
        }
        else
        {
             [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
            
        }
    }
}

#pragma mark- WebServer API
-(void)sentMailToAgent
{
    NSLog(@"%@",arrProperyData);
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    
    NSString *strUserId;
    NSString *strUseremail;
    NSString *strusername;
    NSLog(@"%@",strToEmail);
    if ([strToEmail isEqualToString:@"fromDetail"])
    {
        strUserId=[[dataDict objectForKey:@"User"]objectForKey:@"id"];
        strUseremail=[[dataDict objectForKey:@"Property"]objectForKey:@"email"];
        strusername=[[dataDict objectForKey:@"User"]objectForKey:@"username"];

    }
    else
    {
        strUserId=[[arrProperyData objectAtIndex:0] objectForKey:@"id"];
        strUseremail=[[arrProperyData objectAtIndex:0] objectForKey:@"email"];
        strusername=[[arrProperyData objectAtIndex:0] objectForKey:@"username"];

    }
    
    [params setObject:strUserId forKey:@"propertyid"];
    [params setObject:strUseremail forKey:@"agent_email"];
    [params setObject:strusername forKey:@"agent_name"];
    
    
    [params setObject:[CommonFunctions trimSpaceInString:_enterEmailHere.text] forKey:@"user_email"];
    [params setObject:[CommonFunctions trimSpaceInString:_enterMessageHere.text] forKey:@"user_name"];
    [params setObject:[CommonFunctions trimSpaceInString:_enterName.text] forKey:@"user_phone"];
    [params setObject:[CommonFunctions trimSpaceInString:_enterPhoneNum.text] forKey:@"user_message"];
    [params setObject:language_id forKey:@"language_id"];
    
    DLog(@"%@",params);
    //http://mymeetingdesk.com/mobile/baiti/mobile/mostviewed
    NSString *siteURL = @"contactagent";
    
    
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            if (isarabic)
            {
                [CommonFunctions alertTitleArabic:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil) withDelegate:self withTag:100];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"] withDelegate:self withTag:100];
            }
            
            
        }
        else
        {
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
            
            
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                               
                                          }
                                          
                                      }];
}
- (IBAction)nextButtonClicked:(id)sender {
    [_enterMessageHere becomeFirstResponder];
}
- (IBAction)cancleButtonClicked:(id)sender {
    [self.view endEditing:YES];
}


@end
