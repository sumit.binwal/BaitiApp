//
//  DescriptionVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 01/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DescriptionVC : UIViewController
{
    NSMutableDictionary *dataDict;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *viewForComponents;
@property (strong, nonatomic) IBOutlet NSMutableDictionary *dataDict;


@property (strong, nonatomic) IBOutlet UITextView *txtView1ForFeatures;
@property (strong, nonatomic) IBOutlet UITextView *txtView2ForFeatures;
@property (strong, nonatomic) IBOutlet UITextView *txtViewForDescription;
@property (strong, nonatomic) IBOutlet UITextView *txtView2ForDescrption;
@property (strong, nonatomic) IBOutlet UITextView *txtViewForCommunityHall;

- (IBAction)cameraAction:(id)sender;
- (IBAction)addNotesClickHere:(id)sender;
- (IBAction)goToCallAgent:(id)sender;
- (IBAction)goToEmailAgent:(id)sender;

@property UIImagePickerController *picker;
@property UIImageView* imageView;

@end
