//
//  DescriptionVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 01/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "DescriptionVC.h"
#import "EmailAgentVC.h"
#import "AddNoteVC.h"

@interface DescriptionVC()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    IBOutlet UILabel *lblDescription;
    
}

@end

@implementation DescriptionVC
@synthesize dataDict;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    DLog(@"%@",dataDict);
    lblDescription.text=[[dataDict objectForKey:@"Property"] objectForKey:@"description"];
    self.scrollView.contentSize=_viewForComponents.frame.size;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self scrollViewContent];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setnavigationBar];
    [self setTitle];
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    self.navigationController.navigationBarHidden=NO;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-scroll view content
-(void)scrollViewContent
{
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [_scrollView setScrollEnabled:YES];
        
    }
    else
    {
        [_scrollView setScrollEnabled:NO];
    }
}

#pragma mark-setNavigationbar

- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView)
    {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
    }
    

    titleView.text =LOCALIZATION(@"Description") ;
    self.navigationItem.titleView = titleView;
    
    [titleView sizeToFit];
}

-(void)setnavigationBar
{
    
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0,28,28)];
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
    UIEdgeInsets imageInsets;
    
    if([SYSTEM_VERSION floatValue]>=7.0f)
        imageInsets = UIEdgeInsetsMake(0.0f,-6.0f,0.0f, 00.0f);
    else
        imageInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    leftButton.imageEdgeInsets= imageInsets;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
}
-(void)goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark- Image pIicker delegates

- (IBAction)cameraAction:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        _picker = [[UIImagePickerController alloc] init];
        _picker.delegate = self;
        _picker.allowsEditing = YES;
        _picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:_picker animated:YES completion:NULL];
    }
    else if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
  
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LOCALIZATION(@"Error!")
                                                            message:LOCALIZATION(@"Device has no camera") 
                                                           delegate:nil
                                                  cancelButtonTitle:LOCALIZATION(@"OK")
                                                  otherButtonTitles: nil];
            
            [alert show];
      
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    self.navigationController.navigationBarHidden=NO;
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
}
#pragma mark-goto Add notes Action
- (IBAction)addNotesClickHere:(id)sender
{
    AddNoteVC *addNote=ICLocalizedViewController([AddNoteVC class]);
    
    [self.navigationController pushViewController:addNote animated:YES];
}
#pragma mark-Click to call Action
- (IBAction)goToCallAgent:(id)sender
{
    
    NSString *phNo = [[dataDict objectForKey:@"Property"] objectForKey:@"phone"];
    
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]&&[deviceType isEqualToString:@"iPhone"])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:LOCALIZATION(@"Call facility is not available!!!") delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
#pragma mark-Click to go Email Agent Action
- (IBAction)goToEmailAgent:(id)sender
{
    EmailAgentVC *email=ICLocalizedViewController([EmailAgentVC class]);//[[EmailAgentVC alloc]initWithNibName:@"EmailAgentVC" bundle:nil];
    email.dataDict=dataDict;
    email.strToEmail=@"fromDetail";


    
    [self.navigationController pushViewController:email animated:YES];
    
}
@end
