//
//  ChangePassword.m
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "ChangePassword.h"
#import "HomeVC.h"

@interface ChangePassword ()
{
    
    IBOutlet UIButton *btnSubmit;
}
@end

@implementation ChangePassword

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout=UIRectEdgeNone;
    [self setupView];
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
    }
    else
    {
        isarabic=NO;
    }
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setnavigationBar];
    [self setTitle];
    [self setButtonText];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setButtonText
{
    NSString *string0;
    NSString *string1;
    NSString *text;

        string0 =LOCALIZATION(@"Change") ;
        string1=LOCALIZATION(@"Password");
        
        text = [NSString stringWithFormat:@"%@ %@",string0,string1];
        
    
    
    
    //whole String attribute
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor colorWithRed:110.0f/255.0f green:110.0f/255.0f blue:110.0f/255.0f alpha:1],
                              NSFontAttributeName:[UIFont systemFontOfSize:14]
                              };
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text  attributes:attribs];
    
    NSRange string0Range = [text rangeOfString:string0];
    NSRange string1Range = [text rangeOfString:string1];
    
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Bold" size:18.0f]}  range:string0Range];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Light" size:18.0f]}  range:string1Range];
    
    [btnSubmit setAttributedTitle:attributedText forState:UIControlStateNormal];
    
}


-(void)setupView
{
    UIView *v1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterNewPwd.leftViewMode=UITextFieldViewModeAlways;
    _enterNewPwd.leftView=v1;
    
    UIView *v2=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterOldPasswd.leftViewMode=UITextFieldViewModeAlways;
    _enterOldPasswd.leftView=v2;
    
    UIView *v3=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterPwdForConfirm.leftViewMode=UITextFieldViewModeAlways;
    _enterPwdForConfirm.leftView=v3;
    
}

#pragma mark - set navigationBar
- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
    }
    

    titleView.text =LOCALIZATION(@"Change Password") ;
    self.navigationItem.titleView = titleView;
    
    [titleView sizeToFit];
}
-(void)setnavigationBar
{
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    UIEdgeInsets imageInsets;
    
    if([SYSTEM_VERSION floatValue]>=7.0f)
        imageInsets = UIEdgeInsetsMake(0.0f,-6.0f,0.0f, 00.0f);
    else
        imageInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    leftButton.imageEdgeInsets= imageInsets;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
}
-(void)goBack:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - textField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==self.enterOldPasswd) {
        [_enterOldPasswd resignFirstResponder];
        [_enterNewPwd becomeFirstResponder];
    }
    else if (textField==self.enterNewPwd)
    {
        [_enterNewPwd resignFirstResponder];
        [_enterPwdForConfirm becomeFirstResponder];
    }
    else{
        [_enterPwdForConfirm resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    range = [string rangeOfCharacterFromSet:whitespace];
    if (range.location != NSNotFound)
    {
            // There is whitespace.
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Error!") message:LOCALIZATION(@"Space should not allow in password")  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }
    else
    {
        return YES;
    }
    
}

#pragma mark-textField Validations
-(BOOL)validation
{

        if(![CommonFunctions isValueNotEmpty:_enterOldPasswd.text])
        {
            [_enterOldPasswd becomeFirstResponder];
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter old password")  withDelegate:self];
            return NO;
            
            
        }
        else if(![CommonFunctions isValueNotEmpty:_enterNewPwd.text])
        {
            [_enterNewPwd becomeFirstResponder];
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter new password")  withDelegate:self];
            _enterNewPwd.text=@"";
            return NO;
        }
        else if(![CommonFunctions isValueNotEmpty:_enterPwdForConfirm.text])
        {
            
            [_enterPwdForConfirm becomeFirstResponder];
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter confirm password")  withDelegate:self];
            _enterPwdForConfirm.text=@"";
            return NO;
        }
        
        else if(![_enterNewPwd.text isEqualToString:_enterPwdForConfirm.text])
        {
            [_enterPwdForConfirm becomeFirstResponder];
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Password and confirm password not match")  withDelegate:self];
            _enterPwdForConfirm.text=@"";
            return NO;
        }
     
    
    return YES;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==100) {
        
        HomeVC *home = ICLocalizedViewController([HomeVC class]);//[[HomeVC alloc]initWithNibName:@"HomeVC" bundle:nil];
        [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
        [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
        [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
        }];
    }
}
- (IBAction)changePasswordAction:(id)sender {
    
    if ([self validation]) {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self changePassword];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
        }
    }
}

#pragma mark- WebService API

-(void)changePassword
{
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/changePassword
    NSString *siteURL = @"changePassword";
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN], @"token",nil];
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[CommonFunctions trimSpaceInString:_enterOldPasswd.text], @"old_password",[CommonFunctions trimSpaceInString:_enterPwdForConfirm.text],@"new_password",language_id,@"language_id",nil];
    
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            
            if (isarabic)
            {
                [CommonFunctions alertTitleArabic:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg" ],nil) withDelegate:self withTag:100];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"] withDelegate:self withTag:100];
            }
            
        }
        else
        {
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                              
                                          }
                                          
                                      }];
}
@end
