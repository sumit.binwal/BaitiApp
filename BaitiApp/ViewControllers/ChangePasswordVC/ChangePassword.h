//
//  ChangePassword.h
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePassword : UIViewController<UIAlertViewDelegate,UITextFieldDelegate>
{
    UITextField * activetxtField;
    BOOL isarabic;
}

@property (strong, nonatomic) IBOutlet UITextField *enterOldPasswd;
@property (strong, nonatomic) IBOutlet UITextField *enterNewPwd;
@property (strong, nonatomic) IBOutlet UITextField *enterPwdForConfirm;

- (IBAction)changePasswordAction:(id)sender;

@end
