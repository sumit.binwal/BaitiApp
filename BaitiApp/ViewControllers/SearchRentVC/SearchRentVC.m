//
//  SearchRentVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 01/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "SearchRentVC.h"


@interface SearchRentVC ()

@end

@implementation SearchRentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    // Do any additional setup after loading the view fro
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [self setnavigationBar];
    [self setTitle];
}

#pragma mark - set NavigationBar
- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        
    }

    titleView.text =LOCALIZATION(@"To Rent") ;

    
    [titleView sizeToFit];
}
-(void)setnavigationBar
{
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
}
-(void)goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark -
#pragma mark - tableViewDelegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 192.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifair=@"UITableViewCell";
    
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifair];
    
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifair];
    }
    
    
    // cell.textLabel.text=[self.arrayForTypeOfFlats objectAtIndex:indexPath.row];;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell* )cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

@end
