//
//  AddNoteVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 31/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNoteVC : UIViewController
{
    NSString *propertyID;
    BOOL isArabic;
}
@property (strong, nonatomic) IBOutlet UITextView *addNotesTextView;
@property (strong, nonatomic) NSString *propertyID;
- (IBAction)saveThisNotes:(id)sender;
- (IBAction)cancelToSaveThisNotes:(id)sender;


@end
