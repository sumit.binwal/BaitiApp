//
//  AddNoteVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 31/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "AddNoteVC.h"


@interface AddNoteVC ()<UIAlertViewDelegate>
{
    IBOutlet NSLayoutConstraint *textViewHeightConstraint;
    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *saveChngeButton;
}
@end

@implementation AddNoteVC
@synthesize propertyID;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    if ([UIScreen mainScreen].bounds.size.width>568.0f) {
        textViewHeightConstraint.constant=180;
    }
    
    NSString *lang = [[NSUserDefaults standardUserDefaults]
                      objectForKey:@"ICPreferredLanguage"];
    if ([lang isEqualToString:@"ar"])
    {
        isArabic=YES;
    }
    else
    {
        isArabic =NO;
    }
    [self setUpView];
}


-(void)setUpView
{
    NSString *string0;
    NSString *string1;
    NSString *text;

        string0 = LOCALIZATION(@"Save") ;
        string1=LOCALIZATION(@"Notes") ;
        
        text = [NSString stringWithFormat:@"%@ %@",string0,string1];
            [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    
    //whole String attribute
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor colorWithRed:110.0f/255.0f green:110.0f/255.0f blue:110.0f/255.0f alpha:1],
                              NSFontAttributeName:[UIFont systemFontOfSize:14]
                              };
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
    
    NSRange string0Range = [text rangeOfString:string0];
    NSRange string1Range = [text rangeOfString:string1];
    
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Bold" size:18.0f]}  range:string0Range];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Light" size:18.0f]}  range:string1Range];
    
    [saveChngeButton setAttributedTitle:attributedText forState:UIControlStateNormal];



}

-(void)viewWillAppear:(BOOL)animated
{
    [_addNotesTextView becomeFirstResponder];
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getNotes];
    }
    else
    {
         [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")  withDelegate:self withTag:100];        
    }
    [self setTitle];
    [self setnavigationBar];
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-setNavigationbar

- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView)
    {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        
    }
    
        titleView.text =LOCALIZATION(@"Notes") ;
    
    
    [titleView sizeToFit];
}
-(void)setnavigationBar
{
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 28, 28)];
    [leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
    UIEdgeInsets imageInsets;
    if([SYSTEM_VERSION floatValue]>=7.0f)
        imageInsets = UIEdgeInsetsMake(0.0f,-6.0f,0.0f, 00.0f);
    else
        imageInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    leftButton.imageEdgeInsets= imageInsets;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    
    UIButton * rightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 62, 23)];
    [rightButton addTarget:self action:@selector(saveNote:) forControlEvents:UIControlEventTouchUpInside];
    
    if (isArabic)
    {
        [rightButton setTitle:@"حفظ" forState:UIControlStateNormal];
    }
    else
    {
        [rightButton setTitle:@"Save" forState:UIControlStateNormal];
    }
    
    rightButton.titleLabel.font=[UIFont fontWithName:@"OXYGEN-REGULAR" size:12];
    [rightButton setBackgroundImage:[UIImage imageNamed:@"search_filter_applybut"] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:rightButton];
    
    
}
-(void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)saveNote:(id)sender
{
    [_addNotesTextView resignFirstResponder];
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self saveNotes];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
    }
    
}
#pragma mark-textView Delegatemethod
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"])
    {
        [self.view endEditing:YES];
    }
    
    return YES;
}
#pragma mark-Save notes Action
- (IBAction)saveThisNotes:(id)sender
{
    [self.view endEditing:YES];
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self saveNotes];
    }
    else
    {
         [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];        
    }
    
    
    
}
#pragma mark-Cancel notes Action
- (IBAction)cancelToSaveThisNotes:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark- AddNote API

#pragma mark- WebService API
-(void)getNotes
{
    
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/viewnote
    NSString *siteURL = @"viewnote";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyID, @"propertyid",nil];
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN], @"token",language_id,@"language_id",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            _addNotesTextView.text=[responseDict objectForKey:@"description"];
        }
        else
        {
            if (isArabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
            
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isArabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
                                          }
                                          
                                      }];
}

-(void)saveNotes
{
    
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/addnote
    NSString *siteURL = @"addnote";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyID, @"propertyid",_addNotesTextView.text,@"description",@"1",@"noteid",language_id,@"language_id",nil];
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN], @"token",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            if (isArabic)
            {
                [CommonFunctions alertTitleArabic:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil) withDelegate:self withTag:100];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"] withDelegate:self withTag:100];
            }
            
            
            
        }
        else
        {
            
            
            
            if (isArabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
            
            
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isArabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                             [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                              
                                          }
                                          
                                      }];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==100) {
        
        [self.navigationController popViewControllerAnimated:NO];
    }
}
@end
