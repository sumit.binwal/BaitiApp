//
//  MyFavouritesVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 01/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "MyFavouritesVC.h"
#import "SalesFiltersVC.h"
#import "SearchRentVC.h"
#import "DetailVC.h"
@interface MyFavouritesVC ()
{
    NSMutableArray *favouritesDataArr;
    int currentPage;
    int totalRecord;
    int totalPage;
    NSString *propertyType;
    int apiCurrentpage;
    NSString *propertyID;
    NSString *propertyOwnerID;
    IBOutlet UILabel *lblMessage;
}
@end

@implementation MyFavouritesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    favouritesDataArr =[[NSMutableArray alloc]init];
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
    }
    else
    {
        isarabic=NO;
    }
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setnavigationBar];
    [self setTitle];
    
    
    totalRecord=1;
    _segmentControl.selectedSegmentIndex=0;
    
    propertyType=@"1";
    currentPage=1;
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getFavouritesList];
    }
    else
    {
         [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
        
    }
    
    
    
    
}
#pragma mark-
#pragma mark-setNavigationbar

- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        
    }
    if (isarabic)
    {
        titleView.text = @" لي لمفضلة";
    }
    else
    {
        titleView.text = @"My Favourites";
    }
    
    
    [titleView sizeToFit];
}
-(void)setnavigationBar
{
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftButton addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"list_icon"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    UIEdgeInsets imageInsets;
    
    if([SYSTEM_VERSION floatValue]>=7.0f)
        imageInsets = UIEdgeInsetsMake(0.0f,-6.0f,0.0f, 00.0f);
    else
        imageInsets = UIEdgeInsetsMake(0.0f, -6.0f, 0.0f, 0.0f);
    leftButton.imageEdgeInsets= imageInsets;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    
}
#pragma mark -
#pragma mark - tableViewDelegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return favouritesDataArr.count;
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyFavCustomTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MyFavCustomTableViewCell"];
    
    if (cell==nil) {
        cell = [[MyFavCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyFavCustomTableViewCell"];
        
        
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    [cell.imageForBuilderName setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[favouritesDataArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    cell.priceLbl.text=[NSString stringWithFormat:@"KWD %@",[[[favouritesDataArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"price"]];
    NSString *strRooms=[[[favouritesDataArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"no_of_bedroom"];
    cell.lblForLocation.text=NSLocalizedString([[[favouritesDataArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"location"], nil);
    
    NSString *strPropertyType=[[[favouritesDataArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"property_type"];
    
    if (isarabic)
    {
        cell.lblBed.text=@"قاع";
        cell.noOfRooms.text=[NSString stringWithFormat:@"%@  %@",@"قاع",strRooms];
    }
    else
    {
        cell.lblBed.text=@"Bed";
         cell.noOfRooms.text=[NSString stringWithFormat:@"%@ %@",strRooms,@"Bed"];
    }
    cell.typeOfFlats.text=LOCALIZATION(strPropertyType) ;
    [cell.lblBedNums setText:@"dssgsgs"];
    
    cell.rightButtons = @[ [MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageNamed:@"delete_icon"] backgroundColor:[UIColor colorWithRed:239.0/255.0 green:2.0/255 blue:125.0/255.0 alpha:1.0] callback:^BOOL(MGSwipeTableCell *sender) {
        propertyOwnerID=[[[favouritesDataArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"user_id"];
        propertyID=[[[favouritesDataArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"id"];
        
        [self favWebAPI];
        [favouritesDataArr removeObject:[favouritesDataArr objectAtIndex:indexPath.row]];
        
        return  YES;
    }]];
    
    
  
    
    
    cell.rightSwipeSettings.transition = MGSwipeTransitionDrag;
    
    if ((indexPath.row == [favouritesDataArr count]-1) && ([favouritesDataArr count] != totalRecord))
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        
        [self getFavouritesList];
    }
    
    
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    propertyID=[[[favouritesDataArr objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"id"];
    propertyOwnerID=[[[favouritesDataArr objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"user_id"];
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getPropertyDetails];
    }
    else
    {
         [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
        
    }
    
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(MyFavCustomTableViewCell* )cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CGFloat  height=73;
    return height;
}
-(BOOL)swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction
{
    return YES;
}
-(void) swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState) state gestureIsActive:(BOOL) gestureIsActive
{
    
}

#pragma mark - SegementControl

- (IBAction)ForsaleAndRent:(id)sender {
    
    if(_segmentControl.selectedSegmentIndex == 0)
    {
        propertyType=@"1";
        totalPage=1;
        currentPage=1;
        [favouritesDataArr removeAllObjects];
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getFavouritesList];
        
    }
    else if(_segmentControl.selectedSegmentIndex == 1)
    {
        [favouritesDataArr removeAllObjects];
        propertyType=@"0";
        currentPage=1;
        totalPage=1;
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getFavouritesList];
    }
    
}

#pragma mark - WebService API
-(void)getFavouritesList
{
    //    [lblNoRecord setHidden:YES];
    if(totalRecord==[favouritesDataArr count])
    {
        if (apiCurrentpage==totalPage)
        {
            [CommonFunctions removeActivityIndicator];
            return;
        }
    }
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyType, @"property_mode",[NSString stringWithFormat:@"%d",currentPage],@"pagenumber",nil];
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN], @"token",nil];
    
    
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/myfavouritelist
    NSString *siteURL = @"myfavouritelist";
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)
        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_fav_property"] forKey:UD_FAV_PROPERTY];
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_listed_property"] forKey:UD_LISTED_PROPERTY];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            if ([[[responseDict objectForKey:@"totalRecord"] stringValue] isEqualToString:@"0"]) {
                
                if (isarabic)
                {
                    lblMessage.text=@"لا يوجد معلومات";
                }
                else
                {
                    lblMessage.text=[responseDict objectForKey:@"replyMsg"];
                }
                
                
                [_tableView reloadData];
            }
            else
            {
                lblMessage.text=@"";
                
                apiCurrentpage=[[responseDict objectForKey:@"currentPage"]intValue];
                totalPage=[[responseDict objectForKey:@"totalPages"]intValue];
                totalRecord=[[responseDict objectForKey:@"totalRecord"]intValue];
                
                if ([self isNotNull:[responseDict objectForKey:@"data"]])
                {
                    
                    NSMutableArray *tempArr = [[NSMutableArray alloc]init];
                    // totalPage = [[responseDict objectForKey:@"totalPages"] intValue];
                    
                    NSMutableArray *arr = [[responseDict objectForKey:@"data"] mutableCopy];
                    
                    for (int i = 0; i < [arr count]; i++)
                    {
                        [tempArr addObject:[arr objectAtIndex:i]];
                        
                    }
                    
                    
                    if (currentPage == 1)
                    {
                        [favouritesDataArr removeAllObjects];
                        
                    }
                    
                    for (int i = 0; i < [tempArr count]; i++)
                    {
                        [favouritesDataArr addObject:[tempArr objectAtIndex:i]];
                    }
                    
                    [tempArr removeAllObjects];
                    
                    tempArr = nil;
                    
                    
                    currentPage++;
                    
                    // isLoading = NO;
                    
                }
                
                
                [_tableView reloadData];
            }
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                               
                                          }
                                          
                                      }];
}

-(void)getPropertyDetails
{
    
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/propertydetail
    NSString *siteURL = @"propertydetail";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyID, @"propertyid",propertyOwnerID,@"property_owner_id",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:[[[responseDict objectForKey:@"data"]objectForKey:@"Property"]objectForKey:@"isFav"]  forKey:UD_FAVORITE];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            DetailVC *dtvc=ICLocalizedViewController([DetailVC class]);//[[DetailVC alloc]initWithNibName:@"DetailVC" bundle:nil];
            dtvc.detailDataDict=[responseDict objectForKey:@"data"];
            [self.navigationController pushViewController:dtvc animated:YES];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:[[[responseDict objectForKey:@"data"]objectForKey:@"Property"]objectForKey:@"isFav"]  forKey:UD_FAVORITE];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
            
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                               
                                          }
                                          
                                      }];
}

-(void)favWebAPI
{
    
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/propertydetail
    NSString *siteURL = @"markasfavourite";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyID, @"propertyid",propertyOwnerID,@"property_owner_id",@"true",@"addremove",language_id,@"language_id",nil];
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN], @"token",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_fav_property"] forKey:UD_FAV_PROPERTY];
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_listed_property"] forKey:UD_LISTED_PROPERTY];
            [[NSUserDefaults standardUserDefaults]synchronize];
            if ([[[responseDict objectForKey:@"my_fav_property"] stringValue] isEqualToString:@"0"]) {
                
         
                lblMessage.text=LOCALIZATION(@"No result found") ;
            
                
                [_tableView reloadData];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_fav_property"] forKey:UD_FAV_PROPERTY];
                [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_listed_property"] forKey:UD_LISTED_PROPERTY];
                [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:UD_FAVORITE];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [_tableView reloadData];
                
                if (isarabic)
                {
                    [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
                }
                else
                {
                    [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
                }
                
            }
            
            
        }
        else
        {
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];            }
            
            
            
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                               
                                          }
                                          
                                      }];
}


@end
