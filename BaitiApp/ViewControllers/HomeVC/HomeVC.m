
//  HomeVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 25/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "HomeVC.h"
#import "DetailVC.h"
#import "SalesFiltersVC.h"
#import "SearchRentVC.h"
#import "LocationCustomCell.h"
#import "AgentsListVC.h"
#import "LocationNameCollectionCell.h"

@interface HomeVC()<UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    IBOutlet UICollectionView *collectionViewLocationName;
    IBOutlet UIButton *doneBtn;
    IBOutlet UIButton *cancelBtn;
    IBOutlet UIView *categoryVw;
    IBOutlet UILabel *lblNoRecord;
    IBOutlet NSLayoutConstraint *tblVwHeightConstraint;
    IBOutlet UISearchBar *searchingBar;
    IBOutlet UIView *searchView;
    NSMutableArray *dataArr;
    NSMutableArray *locationSelectionArr;
    int currentPage;
    IBOutlet UITableView *tblVwLocation;
    int totalPage;
    int apiCurrentPage;
    int totalPost;
    int propertyMode;
    NSString *propertyID;
    NSString *propertyOwnerID;
    IBOutlet NSLayoutConstraint *topSpaceCnstraint;
    BOOL isShowSearchBar;
    NSString *isSearch;
    NSMutableArray *locationArray;
    NSString *locationID;
    CGSize labelSize;
    NSMutableArray *sortingDataArr;
    IBOutlet UIView *pickerView;
    NSString *sortingStr;
    NSString *sortingStrID;
}
@end

@implementation HomeVC
@synthesize arrayForBackGroundImages;

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArr=[[NSMutableArray alloc]init];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.homeTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.navigationController.navigationBar setTranslucent:NO];
    currentPage=1;
    apiCurrentPage=1;
    [self storeLoocationInDB];
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        [cancelBtn setTitle:@"الغاء" forState:UIControlStateNormal];
        [doneBtn setTitle:@"منجز" forState:UIControlStateNormal];
        
        isarabic=YES;
    }
    else
    {
  
        isarabic=NO;
    }
    
    searchingBar.placeholder=LOCALIZATION(@"Search Property");
    
    
    sortingDataArr=[[NSMutableArray alloc]initWithObjects:@"Price high to low",@"Price low to high",@"Recent added properties",@"Most viewed properties",nil];
    
    [self setUpView];
    propertyMode=1;
    
    [collectionViewLocationName registerNib:[UINib nibWithNibName:@"LocationNameCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"LocationNameCollectionCell"];
    
    locationSelectionArr =[[NSMutableArray alloc]init];
    isShowSearchBar=false;
    isFromSearch=NO;
    hasSearchResults =NO;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    isSearch=@"0";
    if (isFromSearch)
    {
        if([CommonFunctions reachabiltyCheck])
        {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self getPropertyList];
            //isFromSearch=NO;
        }
        else
        {

        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please Please check your network connection or try again later")  withDelegate:self];
        }
    }
    else
    {
        if([CommonFunctions reachabiltyCheck])
        {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self getPropertyList];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please Please check your network connection or try again later")  withDelegate:self];
        }
        isFromSearch=NO;
    }
    //  DLog(@"&&&&&&& %@",[filterDataArr objectAtIndex:0]);
    [super viewWillAppear:YES];
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [self setUpView];
    //
    //    self.bgImage.image = [UIImage imageNamed:NSLocalizedString(@"tab_bg", nil)];
    //    self.imgRentButtonArrow.image=[UIImage imageNamed:NSLocalizedString(@"right_arrow", nil)];
    //    self.imgSalebtnArrow.image=[UIImage imageNamed:NSLocalizedString(@"right_arrow", nil)];
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-setNavigationbar

-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];

        [self.navigationItem setTitle:LOCALIZATION(@"Home") ];
    
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftButton addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"list_icon"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
    UIButton *searchRightBtn1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [searchRightBtn1 setImage:[UIImage imageNamed:@"search_icon"] forState:UIControlStateNormal];
    [searchRightBtn1 addTarget:self action:@selector(goToSearch:) forControlEvents:UIControlEventTouchUpInside];
    [searchRightBtn1 setFrame:CGRectMake(18, 0, 30, 30)];
    
    UIButton *filterRightBtn2 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [filterRightBtn2 setImage:[UIImage imageNamed:@"filter_icon"] forState:UIControlStateNormal];
    [filterRightBtn2 addTarget:self action:@selector(clickToFilter:) forControlEvents:UIControlEventTouchUpInside];
    [filterRightBtn2 setFrame:CGRectMake(35, 0, 50, 30)];
    
    UIButton *filterRightBtn3 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [filterRightBtn3 setImage:[UIImage imageNamed:@"sorticon1"] forState:UIControlStateNormal];
    [filterRightBtn3 addTarget:self action:@selector(clickToScndFilter:) forControlEvents:UIControlEventTouchUpInside];
    [filterRightBtn3 setFrame:CGRectMake(70, 0, 40, 30)];

    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 110, 32)];
    [rightBarButtonItems addSubview:searchRightBtn1];
    [rightBarButtonItems addSubview:filterRightBtn2];
    [rightBarButtonItems addSubview:filterRightBtn3];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
}

-(void)goToSearch:(id)sender
{
    [self.view endEditing:YES];
    isFromSearch=false;
    if(isShowSearchBar)
    {
        [topSpaceCnstraint setConstant:0.0f];
        isShowSearchBar=false;
        currentPage=1;
        apiCurrentPage=1;
        isSearch=@"0";
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getPropertyList];
    }
    else
    {
        [topSpaceCnstraint setConstant:44.0f];
        isShowSearchBar=true;
    }
}
-(void)clickToScndFilter:(id)sender
{

    pickerView.frame =  CGRectMake(0, self.view.frame.size.height-pickerView.frame.size.height, [UIScreen mainScreen].bounds.size.width, pickerView.frame.size.height);


        [self.view addSubview:pickerView];
  
    pickerView.userInteractionEnabled=YES;
    self.homeTableView.userInteractionEnabled=NO;
    categoryVw.userInteractionEnabled=NO;
    searchView.userInteractionEnabled=NO;
    self.navigationController.navigationBar.userInteractionEnabled=NO;
}
- (IBAction)barCancelBtnClicked:(id)sender
{
    [pickerView removeFromSuperview];
    self.homeTableView.userInteractionEnabled=YES;
    categoryVw.userInteractionEnabled=YES;
    searchView.userInteractionEnabled=YES;
    self.navigationController.navigationBar.userInteractionEnabled=YES;
//    sortingStr=@"";
//    sortingStrID=@"";
}

- (IBAction)barDoneButtonClicked:(id)sender {
    [pickerView removeFromSuperview];
    self.homeTableView.userInteractionEnabled=YES;
    categoryVw.userInteractionEnabled=YES;
    searchView.userInteractionEnabled=YES;
    self.navigationController.navigationBar.userInteractionEnabled=YES;
    if (sortingStr.length<1)
    {
        sortingStr=LOCALIZATION([sortingDataArr objectAtIndex:0]) ;
        sortingStrID=[NSString stringWithFormat:@"%lu",(unsigned long)[sortingDataArr indexOfObject:sortingStr]];
    }
    currentPage=1;
    apiCurrentPage=0;
    if ([CommonFunctions reachabiltyCheck]) {
        
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getPropertyList];
        [_homeTableView reloadData];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")  withDelegate:self];
    }
    NSLog(@"Sorting String ID : --- %@",sortingStrID);
}
-(void)clickToFilter:(id)sender
{
    [self.view endEditing:YES];
    [topSpaceCnstraint setConstant:0.0f];
    isShowSearchBar=false;
    SalesFiltersVC * saleVC=ICLocalizedViewController([SalesFiltersVC class]);//[[SalesFiltersVC alloc]initWithNibName:@"SalesFiltersVC" bundle:nil];
    DLog(@"%d",propertyMode);
    saleVC.propertyID=[NSString stringWithFormat:@"%d",propertyMode];
    saleVC.filterDelegate=self;
    saleVC.locationArr = locationSelectionArr;
    if (hasSearchResults)
    {
        saleVC.dictaFilter=dataFilter;
        NSLog(@"dict:%@",dataFilter);
    }
    else
    {
        
    }
    [self.navigationController pushViewController:saleVC animated:YES];
}

#pragma mark- CollectionView Delegate methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return locationSelectionArr.count;
    
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LocationNameCollectionCell *cell = (LocationNameCollectionCell *)[collectionViewLocationName dequeueReusableCellWithReuseIdentifier:@"LocationNameCollectionCell" forIndexPath:indexPath];
    [cell.lblLocationName setText:[NSString stringWithFormat:@"%@",[[locationSelectionArr objectAtIndex:indexPath.row]
                                                                    objectForKey:@"address"]]];
    DLog(@"%@",locationSelectionArr);
    
    [cell.lblLocationName sizeToFit];
    
    //get the width and height of the label (CGSize contains two parameters: width and height)
    labelSize= cell.lblLocationName.frame.size;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    searchingBar.text=@"";
    [locationSelectionArr removeObject:[locationSelectionArr objectAtIndex:indexPath.row]];
    if ([locationSelectionArr count]<1) {
        [topSpaceCnstraint setConstant:0.0f];
        isSearch=@"0";
        [self.view endEditing:YES];
        currentPage=1;
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getPropertyList];
    }
    else
    {
        isSearch=@"1";
        [self.view endEditing:YES];
        currentPage=1;
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getPropertyList];
    }
    [collectionViewLocationName reloadData];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dict = [locationSelectionArr objectAtIndex:indexPath.row];
    
    CGSize calCulateSizze =[(NSString*)[dict objectForKey:@"address"] sizeWithAttributes:NULL];
    
    DLog(@"%f-%f",calCulateSizze.height, calCulateSizze.width);
    
    calCulateSizze.width = calCulateSizze.width+60;
    
    calCulateSizze.height = 30.0f;
    DLog(@"size -----%f",calCulateSizze.width);
    return calCulateSizze;
}

#pragma mark - tableViewDelegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView==tblVwLocation) {
        return [locationArray count];
    }
    else
    {
        return [dataArr count];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tblVwLocation)
    {
        return 30.0f;
    }
    else
    {
        return 197.0f;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_homeTableView)
    {
        _homeTableView.userInteractionEnabled=YES;
        static NSString *cellIdentifair=@"HomeTableViewCell";
        [self.view endEditing:YES];
        
        HomeTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifair];
        
        if(cell==nil)
        {
            cell=[[HomeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifair];
        }
        
        UIImageView *imageSeparator=[[UIImageView alloc]initWithFrame:CGRectMake(0,cell.contentView.frame.size.height - 1.0, self.view.frame.size.width,2.9)];
        imageSeparator.backgroundColor=[UIColor whiteColor];
        //  cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell.contentView addSubview:imageSeparator];
        
        
        [cell.BackGroundImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[dataArr objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        [[[dataArr objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"property_type"];
        [cell.labelForPrice setText:[NSString stringWithFormat:@"KWD %@",[[[dataArr objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"price"]]];
        [cell.labelForNumOfRooms setText:[[[dataArr objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"no_of_bedroom"]];
        
        if (isarabic)
        {
            cell.lblBed.text=@"السرير";
        }
        else
        {
            
        }
        
        if ([self isNotNull:[[[dataArr objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"location"]]){
            [cell.labelForLocation setText:[[[dataArr objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"location"]];

        }
        else
        {
            [cell.labelForLocation setText:@""];

        }
        if ([self isNotNull:[[[dataArr objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"property_type"]])
        {
            NSString *strName=[[[dataArr objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"property_type"];
            
            
            [cell.typesOfFlats setTitle:LOCALIZATION(strName)  forState:UIControlStateNormal];
        }
        else
        {
            [cell.typesOfFlats setTitle:@"" forState:UIControlStateNormal];
        }
        
        
        [cell.imageForLogo sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[dataArr objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"companylogo"]]] forState:UIControlStateNormal];
        
        
        [cell.button setTitle:[NSString stringWithFormat:@"%@",[[[dataArr objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"total_images_of_property"]] forState:UIControlStateNormal];
        
        
        
        cell.imageForLogo.layer.masksToBounds = YES;
        cell.imageForLogo.layer.cornerRadius = cell.imageForLogo.bounds.size.width/2.0f;
        
        //    if ((indexPath.row == [dataArr count]-1) && ([dataArr count] != totalPosts) && !isLoading)
        //    {
        //        [self getPropertyList];
        //    }
        if ((indexPath.row == [dataArr count]-1) && ([dataArr count] != totalPost))
        {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self getPropertyList];
        }
        return cell;
    }
    else
    {
        _homeTableView.userInteractionEnabled=NO;
        static NSString *cellIdentifair=@"MenuCustemCell";
        LocationCustomCell *cell=[tblVwLocation dequeueReusableCellWithIdentifier:cellIdentifair];
        if(cell==nil)
        {
            cell=[[LocationCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifair];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.lblName.text=[[locationArray objectAtIndex:indexPath.row]objectForKey:@"locationName"];
        return cell;
    }
    return nil;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==_homeTableView)
    {
        propertyOwnerID=[NSString stringWithFormat:@"%@",[[[dataArr objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"user_id"]];
        propertyID=[NSString stringWithFormat:@"%@",[[[dataArr objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"id"]];
        if([CommonFunctions reachabiltyCheck])
        {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self getPropertyDetails];
        }
        else
        {

                [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please Please check your network connection or try again later")  withDelegate:self];
        }
    }
    else
    {
        [self.view endEditing:YES];
        searchingBar.text=@"";
        _homeTableView.userInteractionEnabled=YES;
        [tblVwLocation setHidden:YES];
        NSMutableDictionary *locationDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[locationArray objectAtIndex:indexPath.row] objectForKey:@"locationName"] ,@"address",[[locationArray objectAtIndex:indexPath.row] objectForKey:@"locationID"] ,@"locationId", nil];
        BOOL isFoundValue=true;
        if ([locationSelectionArr count]>0) {
            for (int i = 0; i < [locationSelectionArr count]; i++) {
                
                if ([[[locationSelectionArr objectAtIndex:i]objectForKey:@"address"]isEqualToString:[locationDict objectForKey:@"address"]])
                {
                    isFoundValue=false;
                    return;
                }
            }
            
        }
        if (isFoundValue) {
            [locationSelectionArr addObject:locationDict];
        }
        isSearch=@"1";
        
        DLog(@"--- %@",locationSelectionArr);
        currentPage=1;
        apiCurrentPage=0;
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getPropertyList];
        [topSpaceCnstraint setConstant:75.0f];
        [collectionViewLocationName reloadData];
    }
}

#pragma mark- SearchBar Delegate Methods.

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (text.length == 0)
    {
        [tblVwLocation setHidden:YES];
        return YES;
    }
    else
    {
        if(range.location>0)
        {
            NSString *substring = [NSString stringWithString:searchingBar.text];
            substring = [substring
                         stringByReplacingCharactersInRange:range withString:text];
//            NSLog(@"%@",[[DBManager getSharedInstance] fetchLocation:substring]);
            [self locationResponse:[[DBManager getSharedInstance] fetchLocation:substring]];
//            [self performSelectorOnMainThread:@selector(getLocation:) withObject:substring waitUntilDone:YES];
        }
        return YES;
    }
    
}
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if ([searchBar.text length] < 1)
    {
        [tblVwLocation setHidden:YES];
    }
    
    return YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText length] <= 1)
    {
        [tblVwLocation setHidden:YES];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
    [topSpaceCnstraint setConstant:0.0f];
    currentPage=1;
    isShowSearchBar=false;
    isSearch=@"0";
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getPropertyList];
    }
    else
    {
         [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
    }
}
#pragma mark- Filter results
-(void)setfilteredResults:(NSMutableDictionary *)datatDict
{
    dataFilter=datatDict;
    currentPage=1;
    apiCurrentPage=1;
    isFromSearch=YES;
    hasSearchResults=YES;
}

#pragma mark- IBAction Buttons
- (IBAction)btnForSalePrssed:(id)sender {
    [self.view endEditing:YES];
        [self.bgImage setImage:[UIImage imageNamed:@"tab_bg"]];
    
    if (locationSelectionArr.count>0) {
        
        isSearch=@"1";
         propertyMode=1;
         apiCurrentPage=0;
        [self.view endEditing:YES];
        currentPage=1;
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getPropertyList];
        
    }
    else
    {
        propertyMode=1;
        currentPage=1;
        apiCurrentPage=0;
        isSearch=@"0";
        isFromSearch=NO;
        hasSearchResults=NO;
        
        if ([CommonFunctions reachabiltyCheck]) {
            
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self getPropertyList];
            [_homeTableView reloadData];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")  withDelegate:self];
        }
    }
}

- (IBAction)btnForRentPressed:(id)sender {
    [self.view endEditing:YES];
    [self.bgImage setImage:[UIImage imageNamed:@"rentSlected"]];

    if (locationSelectionArr.count>0) {
        isSearch=@"1";
        propertyMode=0;
        apiCurrentPage=0;
        [self.view endEditing:YES];
        currentPage=1;
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getPropertyList];

    }
    else
    {
        propertyMode=0;
        currentPage=1;
        apiCurrentPage=0;
        isSearch=@"0";
        isFromSearch=NO;
        
        hasSearchResults=NO;
        
        if ([CommonFunctions reachabiltyCheck]) {
            
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self getPropertyList];
            [_homeTableView reloadData];
        }
        else
        {
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")  withDelegate:self];
        }
    }
}
#pragma mark - WebService API
-(void)getPropertyList
{
    [lblNoRecord setHidden:YES];
    if(totalPost==[dataArr count] || !isFromSearch)
    {
        if (apiCurrentPage==totalPage)
        {
            [CommonFunctions removeActivityIndicator];
            return;
        }
    }
    
    NSMutableDictionary *params;
    if ([isSearch isEqualToString:@"1"]) {
        NSString *locationIndex;
        locationIndex=@"";
        
        NSLog(@"%@",[[locationSelectionArr objectAtIndex:0] objectForKey:@"locationId"]);
        for (int i=0; i<locationSelectionArr.count; i++) {
            if (locationIndex.length>1) {
                locationIndex=[locationIndex stringByAppendingString:[NSString stringWithFormat:@",%@",[[locationSelectionArr objectAtIndex:i] objectForKey:@"locationId"]]];
            }
            else
            {
            locationIndex=[locationIndex stringByAppendingString:[NSString stringWithFormat:@"%@",[[locationSelectionArr objectAtIndex:i] objectForKey:@"locationId"]]];
            }
        }
        NSLog(@"%@",locationIndex);
        
        
        if (sortingStr.length>0)
        {
            params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",propertyMode], @"property_mode",[NSString stringWithFormat:@"%d",currentPage],@"pagenumber",@"1",@"search",locationIndex,@"location_id",language_id,@"language_id",sortingStrID,@"sortby",nil];
            
        }
        else
        {
            params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",propertyMode], @"property_mode",[NSString stringWithFormat:@"%d",currentPage],@"pagenumber",@"1",@"search",locationIndex,@"location_id",language_id,@"language_id",nil];

        }
    }
    else if (isFromSearch)
    {
                NSMutableArray *locationArr=[dataFilter objectForKey:@"locations"];
        if (sortingStr.length>0)
        {
            params =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"1",@"adfilter",[dataFilter objectForKey:@"created"],@"created",locationArr,@"locations",[dataFilter objectForKey:@"maxbeds"],@"maxbeds",[dataFilter objectForKey:@"maxprice"],@"maxprice",[dataFilter objectForKey:@"minbeds"],@"minbeds",[dataFilter objectForKey:@"minprice"],@"minprice", [dataFilter objectForKey:@"property_mode"],@"property_mode",@"1",@"search",[NSString stringWithFormat:@"%d",currentPage],@"pagenumber",language_id,@"language_id",sortingStrID,@"sortby",nil];
        }
        else
        {
            NSString *createdStr;
            if ([dataFilter objectForKey:@"created"] == NULL) {
                createdStr = @"";
            }
            else{
                createdStr = [dataFilter objectForKey:@"created"];
            }
            
            NSString *maxBedStr;
            if ([dataFilter objectForKey:@"maxbeds"] == NULL) {
                maxBedStr = @"";
            }
            else{
                maxBedStr = [dataFilter objectForKey:@"maxbeds"];
            }
            
            NSString *maxPriceStr;
            if ([dataFilter objectForKey:@"maxprice"] == NULL) {
                maxPriceStr = @"";
            }
            else{
                maxPriceStr = [dataFilter objectForKey:@"maxprice"];
            }
            
            NSString *minBedStr;
            if ([dataFilter objectForKey:@"minbeds"] == NULL) {
                minBedStr = @"";
            }
            else{
                minBedStr = [dataFilter objectForKey:@"minbeds"];
            }
            
            NSString *minPriceStr;
            if ([dataFilter objectForKey:@"minprice"] == NULL) {
                minPriceStr = @"";
            }
            else{
                minPriceStr = [dataFilter objectForKey:@"minprice"];
            }
            
            NSString *propertyModeStr;
            if ([dataFilter objectForKey:@"property_mode"] == NULL) {
                propertyModeStr = @"";
            }
            else{
                propertyModeStr = [dataFilter objectForKey:@"property_mode"];
            }
            
            if (locationArr.count == 0) {
                locationArr = [NSMutableArray array];
            }
            
            params =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"1",@"adfilter",createdStr,@"created",locationArr,@"locations",maxBedStr,@"maxbeds",maxPriceStr,@"maxprice",minBedStr,@"minbeds",minPriceStr,@"minprice", [NSString stringWithFormat:@"%d",propertyMode],@"property_mode",@"1",@"search",[NSString stringWithFormat:@"%d",currentPage],@"pagenumber",language_id,@"language_id",nil];
            NSLog(@"%@",params);
        }
    }
    else
    {
         if (sortingStr.length>0)
        {
            params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",propertyMode], @"property_mode",[NSString stringWithFormat:@"%d",currentPage],@"pagenumber",language_id,@"language_id",sortingStrID,@"sortby",nil];
            NSLog(@"%@",params);
        }
        else
        {
            params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",propertyMode], @"property_mode",[NSString stringWithFormat:@"%d",currentPage],@"pagenumber",language_id,@"language_id",nil];
        }
    }
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/propertysearch
    NSString *siteURL = @"propertysearch";
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            
            apiCurrentPage=[[responseDict objectForKey:@"currentPage"] intValue];
            totalPage=[[responseDict objectForKey:@"totalPages"]intValue];
            totalPost=[[responseDict objectForKey:@"totalRecord"]intValue];
            
            if ([self isNotNull:[responseDict objectForKey:@"data"]])
            {
                
                NSMutableArray *tempArr = [[NSMutableArray alloc]init];
                totalPage = [[responseDict objectForKey:@"totalPages"] intValue];
                
                NSMutableArray *arr = [[responseDict objectForKey:@"data"] mutableCopy];
                
                for (int i = 0; i < [arr count]; i++)
                {
                    [tempArr addObject:[arr objectAtIndex:i]];
                }
                if (currentPage == 1)
                {
                    [dataArr removeAllObjects];
                    
                }
                for (int i = 0; i < [tempArr count]; i++)
                {
                    [dataArr addObject:[tempArr objectAtIndex:i]];
                }
                
                [tempArr removeAllObjects];
                
                tempArr = nil;
                
                DLog(@"user info Arr :%@",dataArr);
                
                currentPage++;
                
                // isLoading = NO;
                [tblVwLocation setHidden:YES];
                
            }
            //            NSMutableArray *tempArr=[[NSMutableArray alloc]init];
            //            for (int i=0; i < [[responseDict objectForKey:@"data"]count]; i++) {
            //            [tempArr addObject:[[responseDict objectForKey:@"data"] objectAtIndex:i]];
            //            }
            //
            //            for (int i=0; i<tempArr.count; i++) {
            //                [dataArr addObject:[tempArr objectAtIndex:i]];
            //            }
            //
            [_homeTableView reloadData];
            
        }
        else
        {
            
            if ([[responseDict objectForKey:@"replyMsg"]isEqualToString:@"No result found"]) {
                [dataArr removeAllObjects];
                [_homeTableView reloadData];
  
                lblNoRecord.text=LOCALIZATION(@"No result found") ;
                [lblNoRecord setHidden:NO];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                               
                                          }
                                          
                                      }];
}

-(void)getPropertyDetails
{
    
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/propertydetail
    NSString *siteURL = @"propertydetail";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyID, @"propertyid",propertyOwnerID,@"property_owner_id",language_id,@"language_id",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:[[[responseDict objectForKey:@"data"]objectForKey:@"Property"]objectForKey:@"isFav"]  forKey:UD_FAVORITE];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            
            DetailVC *dtvc=ICLocalizedViewController([DetailVC class]);
            dtvc.detailDataDict=[responseDict objectForKey:@"data"];
            [self.navigationController pushViewController:dtvc animated:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                               
                                          }
                                          
                                      }];
}

-(void)getLocation:(NSString *)str
{
    DLog(@"-------String : %@",str);
    //http://mymeetingdesk.com/mobile/baiti/mobile/locations

    NSString *siteURL = @"locations";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:str, @"pressed_keyword",language_id,@"language_id",nil];
    
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [self performSelectorOnMainThread:@selector(locationResponse:) withObject:responseDict waitUntilDone:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                           }
                                      }];
}

-(void)storeLoocationInDB
{
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/all_locations
    
    
    NSString *siteURL = @"all_locations";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:language_id,@"language_id",nil];
    
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            NSMutableArray *arrLocation=[responseDict objectForKey:@"data"];
            
            BOOL isTruncate=[[DBManager getSharedInstance] truncateLocationTableData];
            if (isTruncate) {
                NSLog(@"isTruncate Added Scucessfully");
            }
            else
            {
                NSLog(@"Not Added Scucessfully");
            }

            
            BOOL isCreate=[[DBManager getSharedInstance]insertCategory:arrLocation];
            if (isCreate) {
                NSLog(@"Added Scucessfully");
            }
            else
            {
                NSLog(@"Not Added Scucessfully");
            }
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                           }
                                      }];
}


-(void)locationResponse:(NSMutableArray *)locationArr
{
    locationArray=locationArr;

    [tblVwLocation setHidden:NO];
    
    int tblHeight= [UIScreen mainScreen].bounds.size.height-320;
    
    if ([locationArray count]*30>tblHeight) {
        [tblVwLocation setFrame:CGRectMake(searchView.frame.origin.x, searchingBar.frame.origin.y+searchingBar.frame.size.height, [UIScreen mainScreen].bounds.size.width, tblHeight)];
    }
    else
    {
        [tblVwLocation setFrame:CGRectMake(searchView.frame.origin.x, searchingBar.frame.origin.y+searchingBar.frame.size.height,[UIScreen mainScreen].bounds.size.width, [locationArray count]*30)];
    }
    [tblVwLocation reloadData];
    isSearch=@"0";
}

#pragma mark-UIPickerView Delegate methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return sortingDataArr.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return LOCALIZATION([sortingDataArr objectAtIndex:row]);
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
        sortingStr=LOCALIZATION([sortingDataArr objectAtIndex:row]);
        sortingStrID=[NSString stringWithFormat:@"%lu",(unsigned long)[sortingDataArr indexOfObject:sortingStr]];
}
@end
