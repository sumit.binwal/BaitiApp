//
//  SignUpVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 25/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "SignUpVC.h"
#import "HomeVC.h"
#import "IIViewDeckController.h"
@interface SignUpVC ()<UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    NSString *is_subscribe;
    UITextField *activeTextFld;
    IBOutlet UIButton *btnSignUp;
}
@end

@implementation SignUpVC
@synthesize txtFldUserName,txtFldUserEmaiId,txtFldUsePassword,txtFldUseConfrimPassword;
@synthesize scrollview;
@synthesize msgDict;

- (void)viewDidLoad
{
    [super viewDidLoad];
    is_subscribe=0;
    msgDict=[[NSMutableDictionary alloc]init];
    
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [scrollview setScrollEnabled:YES];
        
    }
    else
    {
        [scrollview setScrollEnabled:NO];
    }
 
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureClicked)];
    tapGesture.delegate=self;
    tapGesture.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tapGesture];
    
    [txtFldUseConfrimPassword setValue:[UIColor colorWithRed:74.0f/255.0f green:74.0f/255.0f blue:74.0f/255.0f alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
        [txtFldUsePassword setValue:[UIColor colorWithRed:74.0f/255.0f green:74.0f/255.0f blue:74.0f/255.0f alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
        [txtFldUserEmaiId setValue:[UIColor colorWithRed:74.0f/255.0f green:74.0f/255.0f blue:74.0f/255.0f alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
        [txtFldUserName setValue:[UIColor colorWithRed:74.0f/255.0f green:74.0f/255.0f blue:74.0f/255.0f alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
        
        imgSignUP.image=[UIImage imageNamed:@"Signiv"];
    }
    else
    {
        isarabic=NO;
    }


}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self setButtonText];
}

-(void)tapGestureClicked
{
    [activeTextFld resignFirstResponder];
    [scrollview setContentOffset:CGPointZero];
}


- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [scrollview setContentSize:CGSizeMake(320.0f, 750.0f)];
}

#pragma mark - Action Method

-(IBAction)sinupButton:(UIButton *)sender
{
    [self.view endEditing:YES];
    [scrollview setContentOffset:CGPointZero];
    if ([self validate])
    {
     if([CommonFunctions reachabiltyCheck])
     {
         [CommonFunctions showActivityIndicatorWithText:@""];
         [self userSignUp];
     }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
    }

    }
}

- (void)scrollViewToCenterOfScreen:(UITextField *)txtField
{
    float difference;
    
    if (scrollview.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 60.0f;
    
    CGFloat viewCenterY = txtField.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 50.0f;
    CGFloat y = viewCenterY - avaliableHeight / 4.0f;
    
    if (y < 0)
        y = 0;
    
    [scrollview setContentOffset:CGPointMake(0, y) animated:YES];
    [scrollview setContentSize:CGSizeMake(320.0f, 750.0f)];
}

#pragma mark - TextFiled Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeTextFld=textField;
    [self scrollViewToCenterOfScreen:textField];
    [scrollview setScrollEnabled:YES];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [scrollview setScrollEnabled:YES];
    }
    else
    {
        [scrollview setScrollEnabled:NO];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    switch (textField.tag)
    {
        case 1:
            [txtFldUserEmaiId becomeFirstResponder];
            break;
        case 2:
            
            [txtFldUsePassword becomeFirstResponder];
            break;
            
        case 3:
            
            [txtFldUseConfrimPassword becomeFirstResponder];
            break;
            
        case 4:
            txtFldUseConfrimPassword.text=textField.text;
           [scrollview setContentOffset:CGPointMake(0, 0) animated:YES];
            [self.view endEditing:YES];
            break;
    }
    return YES;
}

-(BOOL)validate
{

        if(![CommonFunctions isValueNotEmpty:txtFldUserName.text])
        {
            
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter name")  withDelegate:self];
            activeTextFld=txtFldUserName;
            return NO;
        }
        
        else if(![CommonFunctions isValueNotEmpty:txtFldUserEmaiId.text])
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter email id")  withDelegate:self];
            activeTextFld=txtFldUserEmaiId;
            return NO;
        }
        else if (![CommonFunctions IsValidEmail:txtFldUserEmaiId.text])
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter valid email")  withDelegate:self];
            activeTextFld=txtFldUserEmaiId;
            return NO;
        }
        
        else if (![CommonFunctions isValueNotEmpty:txtFldUsePassword.text])
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter password!")  withDelegate:self];
            activeTextFld=txtFldUsePassword;
            return NO;
        }
        
        else if (txtFldUsePassword.text.length < 6)
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter 6 digit password")  withDelegate:self];
            activeTextFld=txtFldUsePassword;
            return NO;
        }
        
        else if (![CommonFunctions isValueNotEmpty:txtFldUseConfrimPassword.text])
        {
            
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Confirm password")  withDelegate:self];
            activeTextFld=txtFldUseConfrimPassword;
            return NO;
        }
        
        else if(![txtFldUsePassword.text isEqualToString:txtFldUseConfrimPassword.text])
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Password not match")  withDelegate:self];
            activeTextFld=txtFldUseConfrimPassword;
            return NO;
        }

       return YES;
}



-(void)userSignUp
{
    NSString *useLatitude=[NSString stringWithFormat:@"%f",CurrentLatitude];
    NSString *userLongitude=[NSString stringWithFormat:@"%f",CurrentLongitude];
    
    NSString *siteURL = @"signup";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[CommonFunctions trimSpaceInString:txtFldUserName.text], @"username",[CommonFunctions trimSpaceInString:txtFldUserEmaiId.text], @"email",[CommonFunctions trimSpaceInString:txtFldUseConfrimPassword.text], @"password",useLatitude, @"useLatitude",userLongitude, @"userLongitudee",userDeviceToken, @"deviceToken",is_subscribe, @"is_subscribe",language_id, @"language_id",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        

        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];

            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:responseDict];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:UD_USERINFO];

            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"token"] forKey:UD_TOKEN];
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_fav_property"] forKey:UD_FAV_PROPERTY];
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_listed_property"] forKey:UD_LISTED_PROPERTY];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            HomeVC *resourceVC  = ICLocalizedViewController([HomeVC class]);
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:resourceVC animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:resourceVC]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
            if (isarabic)
            {
                [CommonFunctions alertTitleArabic:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil) withDelegate:self withTag:100];
            }
            else
           {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"] withDelegate:self withTag:100];
            }
            
            
        }
        else
        {
            
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
        }
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                        [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }

                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                   [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")]; 
                                          }
                                          
                                      }];
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {

    IIViewDeckController* deckController = [APPDELEGATE generateControllerStack];
        
        APPDELEGATE.window.rootViewController=deckController;
    }
    else if (activeTextFld==txtFldUserName)
    {
        [txtFldUserName becomeFirstResponder];
    }
    else if (activeTextFld==txtFldUserEmaiId)
    {
        [txtFldUserEmaiId becomeFirstResponder];
    }
    else if (activeTextFld==txtFldUsePassword)
    {
        [txtFldUsePassword becomeFirstResponder];
    }
    else if (activeTextFld==txtFldUseConfrimPassword)
    {
        [txtFldUseConfrimPassword becomeFirstResponder];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)subscribeBtnAction:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    if (btn.tag==0) {
        
        [_subScribeBtn setBackgroundImage:[UIImage imageNamed:@"checked_box"] forState:UIControlStateSelected];
        btn.tag=1;
        is_subscribe=@"1";
        }
    else
    {
        [_subScribeBtn setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateSelected];
        btn.tag=0;
        is_subscribe=@"0";
    }
}


-(void)setButtonText
{
    NSString *string0;
    NSString *string1;
    NSString *text;

        string0 =@"Sign";
        string1=@"Up" ;
        
        text = [NSString stringWithFormat:@"%@ %@",string0,string1];
   
    //whole String attribute
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor whiteColor],
                              NSFontAttributeName:[UIFont systemFontOfSize:14]
                              };
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(text)attributes:attribs];
    
    NSRange string0Range = [text rangeOfString:string0];
    NSRange string1Range = [text rangeOfString:string1];
    
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Bold" size:18.0f]}  range:string0Range];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Light" size:18.0f]}  range:string1Range];
    
    [btnSignUp setAttributedTitle:attributedText forState:UIControlStateNormal];
}

- (IBAction)signInHere:(id)sender {
    SignInVC * signIn=ICLocalizedViewController([SignInVC class]);//[[SignInVC alloc]initWithNibName:@"SignInVC" bundle:nil];
    [self presentViewController:signIn animated:YES completion:nil];
}
- (IBAction)skipButtonClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
