//
//  PostProperty.m
//  BaitiApp
//
//  Created by Shweta Rao on 30/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "PostProperty.h"
#import "ELCImagePickerHeader.h"
#import "LocationCustomCell.h"

@interface PostProperty ()<UIPickerViewDataSource,UIPickerViewDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,ELCImagePickerControllerDelegate,UIScrollViewDelegate>
{
    IBOutlet UIButton *btnNestStep;
    IBOutlet UIImageView *fullScreenImgVw;
    IBOutlet UIButton *fullScreenCrossBtn;
    IBOutlet UIView *fullScreenView;
    IBOutlet UIBarButtonItem *uibarbuttonDone;
    IBOutlet UIView *toolbarVw;
    IBOutlet UIView *customePIckerView;
    IBOutlet UIScrollView *scrllVw;
    NSString *propertyTypeID;
    IBOutlet UITextField *txtPropertieName;
    IBOutlet UIPickerView *pickerVw;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UITableView *tblVw;
    BOOL *textFiledAppear;
    UITapGestureRecognizer *tapGesture;

}

- (IBAction)fullScreenCrossButton:(id)sender;

@end

@implementation PostProperty
@synthesize userlet,locationID,userlong,txtPropertyType,propertyMode,propertyTypeArr,LocationArr,imageArr,enterLocation,enterPrice,detailDataDict;
- (void)viewDidLoad {
    [super viewDidLoad];
    

    DLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"]);
    propertyMode=@"1";
    
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    imageArr =[[NSMutableArray alloc]init];
    
    NSLog(@"-- TOKEN : %@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN]);
    [_collView registerNib:[UINib nibWithNibName:@"PostPropertyCollViewCell" bundle:nil] forCellWithReuseIdentifier:@"PostPropertyCollViewCell"];
    [scrllVw setScrollEnabled:YES];
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [scrllVw setContentSize:CGSizeMake(320.0f, 500.0f)];
    }
    else
    {
        [scrllVw setContentSize:CGSizeMake(320.0f, 560.0f)];
        [scrllVw setScrollEnabled:NO];
        
    }
    
    txtPropertyType.inputView=pickerVw;
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
    }
    else
    {
        isarabic=NO;
    }
    
    
    
    if([CommonFunctions reachabiltyCheck])
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getPropertyType];
    }
    else
    {
         [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")  withDelegate:self withTag:100];
    }
    
    //    tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureClicked)];
    //   tapGesture.numberOfTapsRequired=1;
    //    tapGesture.delegate=self;
    //    [self.view addGestureRecognizer:tapGesture];
    
    UIView *view1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    txtPropertyType.leftViewMode=UITextFieldViewModeAlways;
    txtPropertyType.leftView=view1;
    
    UIView *view2=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    enterLocation.leftViewMode=UITextFieldViewModeAlways;
    enterLocation.leftView=view2;
    
    UIView *view3=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    enterPrice.leftView=view3;
    enterPrice.leftViewMode=UITextFieldViewModeAlways;
    
    [activityIndicator setHidden:YES];
    
    UIView *view4=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    txtPropertieName.leftViewMode=UITextFieldViewModeAlways;
    txtPropertieName.leftView=view4;
    
    
    [customePIckerView setFrame:CGRectMake(customePIckerView.frame.origin.x, customePIckerView.frame.origin.y, [UIScreen mainScreen].bounds.size.width, customePIckerView.frame.size.height)];
    
    enterPrice.inputAccessoryView=toolbarVw;
    txtPropertyType.inputAccessoryView=toolbarVw;
    
    if (detailDataDict.count >0) {
        
        DLog(@"%@",detailDataDict);
        
        if ([[[detailDataDict objectForKey:@"Property"] objectForKey:@"property_mode"]isEqualToString:@"0"]) {
            [_rentBtn setImage:[UIImage imageNamed:@"radio_buttion_selected"] forState:UIControlStateNormal];
            [_saleBtn setImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
            propertyMode=@"0";
        }
        else
        {
            [_saleBtn setImage:[UIImage imageNamed:@"radio_buttion_selected"] forState:UIControlStateNormal];
            [_rentBtn setImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
            propertyMode=@"1";
        }
        propertyTypeID=[[detailDataDict  objectForKey:@"Propertytype"]objectForKey:@"id"];
        userlet=[[detailDataDict objectForKey:@"Property"]objectForKey:@"mlat"];
        userlong=[[detailDataDict objectForKey:@"Property"]objectForKey:@"mlong"];
        
        txtPropertieName.text=[[detailDataDict objectForKey:@"Property"]objectForKey:@"title"];
        txtPropertyType.text=[[detailDataDict  objectForKey:@"Propertytype"]objectForKey:@"title"];
        enterLocation.text=[[detailDataDict  objectForKey:@"Property"]objectForKey:@"location"];
        enterPrice.text=[[detailDataDict  objectForKey:@"Property"]objectForKey:@"price"];
        
        NSMutableArray *tempArr=[detailDataDict objectForKey:@"PropertyImage"];
        for (int i=0;i<tempArr.count;i++)
        {
            NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[tempArr objectAtIndex:i]objectForKey:@"imagename"]]];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            [imageArr addObject:[UIImage imageWithData:imageData]];
        }
        [_collView reloadData];
    }
}

-(void)setButtonText
{
    NSString *string0;
    NSString *string1;
    NSString *text;

        string0 = LOCALIZATION(@"Next") ;
        string1=LOCALIZATION(@"Step") ;
        
        text = [NSString stringWithFormat:@"%@ %@",string0,string1];
        
    
    
    
    //whole String attribute
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor colorWithRed:110.0f/255.0f green:110.0f/255.0f blue:110.0f/255.0f alpha:1],
                              NSFontAttributeName:[UIFont systemFontOfSize:14]
                              };
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
    
    NSRange string0Range = [text rangeOfString:string0];
    NSRange string1Range = [text rangeOfString:string1];
    
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Bold" size:18.0f]}  range:string0Range];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Light" size:18.0f]}  range:string1Range];
    
    [btnNestStep setAttributedTitle:attributedText forState:UIControlStateNormal];
    
}



- (IBAction)barCancelButtonClicked:(id)sender {
    [self.view endEditing:YES];
    [scrllVw setContentOffset:CGPointZero];
    txtPropertyType.text=@"";
}

- (IBAction)doneButtonClicked:(id)sender {
    if(activetxFld==txtPropertyType)
    {
        if(txtPropertyType.text.length<1)
        {
            txtPropertyType.text=[[[propertyTypeArr objectAtIndex:0] objectForKey:@"Propertytype"] objectForKey:@"title"];
            propertyTypeID=[[[propertyTypeArr objectAtIndex:0] objectForKey:@"Propertytype"] objectForKey:@"id"];
        }
        
        [enterLocation becomeFirstResponder];
    }
    else
    {
        [enterPrice resignFirstResponder];
        [scrllVw setContentOffset:CGPointZero];
        
    }
}

//-(void)tapGestureClicked
//{
//    [self.view endEditing:YES];
//   [tblVw setHidden:YES];
//    [scrllVw setContentOffset:CGPointZero];
//    if([UIScreen mainScreen].bounds.size.height<568)
//    {
//        [scrllVw setScrollEnabled:YES];
//    }
//    else
//    {
//        [scrllVw setScrollEnabled:NO];
//    }
//
//}
-(void)viewDidLayoutSubviews
{
    if(textFiledAppear)
    {
        if([UIScreen mainScreen].bounds.size.height<568)
        {
            [scrllVw setContentSize:CGSizeMake(320.0f, 800.0f)];
        }
        else
        {
            [scrllVw setContentSize:CGSizeMake(320.0f, 800.0f)];
        }
    }
    else
    {
        
        if([UIScreen mainScreen].bounds.size.height<568)
        {
            [scrllVw setContentSize:CGSizeMake(320.0f, 500.0f)];
        }
        else
        {
            [scrllVw setScrollEnabled:NO];
            [scrllVw setContentSize:CGSizeMake(320.0f, 560.0f)];
        }
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [self setnavigationBar];
    [self setTitle];
    [self setButtonText];
    // [scrllVw setScrollEnabled:YES];
    
}

#pragma mark-setNavigationbar

- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        
    }

    titleView.text =LOCALIZATION(@"Post Property") ;
    
    [titleView sizeToFit];
}

-(void)setnavigationBar
{
    
    UINavigationBar *navBar=self.navigationController.navigationBar;
    //self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:227.0f/255.0f green:0.0f/255.0f blue:134.0f/255.0f alpha:1];
    navBar.tintColor=[UIColor whiteColor];
    
    UIImage *img=[UIImage imageNamed:@"top_bar_bg"];
    
    [navBar setBackgroundImage:img forBarMetrics:UIBarMetricsDefault];
    [self.navigationController setNavigationBarHidden:FALSE];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftButton addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"list_icon"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
    
}
#pragma mark- collectionViewDelegates

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return imageArr.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PostPropertyCollViewCell *cell = (PostPropertyCollViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"PostPropertyCollViewCell" forIndexPath:indexPath];
    [cell.imgVw setImage:[imageArr objectAtIndex:indexPath.row]];
    [cell.crossButton addTarget:self action:@selector(crossButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(void)crossButtonClicked:(UIButton *)sender
{
    NSIndexPath *indexPath;
    indexPath = [self.collView indexPathForItemAtPoint:[self.collView convertPoint:sender.center fromView:sender.superview]];
    [imageArr removeObjectAtIndex:indexPath.row];
    [self.collView reloadData];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view addSubview:fullScreenView];
    [self fadeInAnimation:self.view];
    fullScreenView.frame=[UIScreen mainScreen].bounds;
    fullScreenImgVw.image=[imageArr objectAtIndex:indexPath.row];
}

-(void)fadeInAnimation:(UIView *)aView {
    CATransition *transition = [CATransition animation];
    transition.type =kCATruncationMiddle;
    transition.duration = 0.5f;
    transition.delegate = self;
    [aView.layer addAnimation:transition forKey:nil];
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //DLog(@"SETTING SIZE FOR ITEM AT INDEX %d", indexPath.row);
    CGSize mElementSize = CGSizeMake(62,62);
    return mElementSize;
}
#pragma mark-textFiled Delegates

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == txtPropertieName) {
        [textField resignFirstResponder];
        [txtPropertyType becomeFirstResponder];
    }
    
    else if(textField == enterLocation)
    {
        [textField resignFirstResponder];
        [enterPrice becomeFirstResponder];
    }
    else if (textField==enterPrice) {
        [textField resignFirstResponder];
        
    }
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    textFiledAppear=0;
    activetxFld=textField;
    [self scrollViewToCenterOfScreen:textField];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    textFiledAppear=false;
    [tblVw setHidden:YES];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField==enterPrice)
    {
        if (range.location >= 15) {
            return NO;
        }
        return YES;
        
    }
    
    else if(textField==enterLocation)
    {
        if (range.location<1)
        {
            
                    [tblVw setHidden:YES];
                    [activityIndicator setHidden:YES];
                    [activityIndicator stopAnimating];
            
        }
        
        

        else if(range.location>0)
        {
           

            NSString *substring = [NSString stringWithString:textField.text];
            substring = [substring
                         stringByReplacingCharactersInRange:range withString:string];
            
        
            tapGesture.enabled=NO;
            
            LocationArr=[[DBManager getSharedInstance] fetchLocation:substring];
            [tblVw setHidden:NO];
            
            
            [tblVw setFrame:CGRectMake(enterLocation.frame.origin.x, enterLocation.frame.origin.y+enterLocation.frame.size.height, [UIScreen mainScreen].bounds.size.width, 200.0f)];
            [tblVw reloadData];
//            [self getLocation:substring];
        }
        return YES;
    }
    else
    {
        return YES;
    }
}


#pragma mark-textField Validations
-(BOOL)validation
{

        if(![CommonFunctions isValueNotEmpty:txtPropertieName.text])
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter Property Title")  withDelegate:self withTag:100];
            return NO;
        }
        else if(![CommonFunctions isValueNotEmpty:txtPropertyType.text])
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please select property Type First")  withDelegate:self withTag:101];
            return NO;
        }
        else if (![CommonFunctions isValueNotEmpty:enterLocation.text])
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter location")  withDelegate:self withTag:102];
            return NO;
        }
        else if (![CommonFunctions isValueNotEmpty:[NSString stringWithFormat:@"%@",locationID]])
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter valid location")  withDelegate:self withTag:102];
            return NO;
        }
        else if (![CommonFunctions isValueNotEmpty:enterPrice.text])
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter price")  withDelegate:self withTag:103];
            return NO;
        }
        else if (imageArr.count<1)
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please add atleast one property image!!")  withDelegate:self withTag:104];
            return NO;
            
        }
        
    
    
    return YES;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==100)
    {
        [txtPropertieName becomeFirstResponder];
    }
    else if(alertView.tag==101)
    {
        //    txtPropertyType.inputView=customePIckerView;
        //        [txtPropertyType becomeFirstResponder];
    }
    else if (alertView.tag==102)
    {
        [enterLocation becomeFirstResponder];
    }
    else if (alertView.tag==103)
    {
        [enterPrice becomeFirstResponder];
    }
    else if (alertView.tag==111)
    {
        IIViewDeckController *controller=[APPDELEGATE generateControllerStack];
        APPDELEGATE.window.rootViewController=controller;
    }
}

- (IBAction)forSale:(id)sender {
    
    [_saleBtn setImage:[UIImage imageNamed:@"radio_buttion_selected"] forState:UIControlStateNormal];
    [_rentBtn setImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
    propertyMode=@"1";
    
}

- (IBAction)forRent:(id)sender {
    
    [_rentBtn setImage:[UIImage imageNamed:@"radio_buttion_selected"] forState:UIControlStateNormal];
    [_saleBtn setImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
    propertyMode=@"0";
}


-(void)scrollViewToCenterOfScreen:(UITextField *)txtField
{
    [scrllVw setScrollEnabled:YES];
    float difference;
    
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 60.0f;
    
    CGFloat viewCenterY = txtField.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 100.0f;
    CGFloat y = viewCenterY - avaliableHeight / 4.0f;
    
    if (y < 0)
        y = 0;
    
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [scrllVw setContentSize:CGSizeMake(320.0f, 800.0f)];
    }
    else
    {
        [scrllVw setContentSize:CGSizeMake(320.0f, 750.0f)];
    }}


#pragma mark - image picker Delegates

- (IBAction)cameraBtnClicked:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        _picker = [[UIImagePickerController alloc] init];
        _picker.delegate = self;
        _picker.allowsEditing = YES;
        _picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:_picker animated:YES completion:NULL];
    }
    else if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {

            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:LOCALIZATION(@"Baiti")
                                                                  message:LOCALIZATION(@"Device has no camera")
                                                                 delegate:nil
                                                        cancelButtonTitle:LOCALIZATION(@"OK")
                                                        otherButtonTitles: nil];
            [myAlertView show];
        
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageView.image = chosenImage;
    [imageArr addObject:chosenImage];
    [_collView reloadData];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)galleryBtnClicked:(id)sender {
    
    
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
    
    elcPicker.maximumImagesCount = 20; //Set the maximum number of images to select to 100
    elcPicker.returnsOriginalImage = NO; //Only return the fullScreenImage, not the fullResolutionImage
    elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
    elcPicker.onOrder = YES; //For multiple image selection, display and return order of selected images
    elcPicker.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie]; //Supports image and movie types
    
    elcPicker.imagePickerDelegate = self;
    
    [self presentViewController:elcPicker animated:YES completion:nil];
}

- (IBAction)goToPostPropertyStep2:(id)sender {
    [self.view endEditing:YES];
    [scrllVw setContentOffset:CGPointZero];
    if ([self validation]) {
        PostPropertyStep2VC *agentList = ICLocalizedViewController([PostPropertyStep2VC class]);//[[PostPropertyStep2VC alloc]initWithNibName:@"PostPropertyStep2VC" bundle:nil];
        
        DLog(@"%@",txtPropertyType.text);
        NSMutableDictionary *postParams=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyMode,@"property_mode",propertyTypeID,@"property_type",locationID,@"location",enterPrice.text,@"price",imageArr,@"picture",[CommonFunctions trimSpaceInString:txtPropertieName.text],@"title", nil];
        agentList.postPropertyDataArr=[[NSMutableArray alloc]initWithObjects:postParams, nil];
        agentList.propertyDataDict=detailDataDict;
        
        NSLog(@"%@",agentList.postPropertyDataArr);
        [(UINavigationController *)self.viewDeckController.centerController pushViewController:agentList animated:YES];
        [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:agentList]];
        [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
        }];
    }
}



#pragma mark ELCImagePickerControllerDelegate Methods
- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    if (imageArr.count<20) {
        CGRect workingFrame = scrllVw.frame;
        workingFrame.origin.x = 0;
        
        NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
        for (NSDictionary *dict in info) {
            if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
                if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                    UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                    [images addObject:image];
                    if (imageArr.count<20) {
                        [imageArr addObject:image];
                            [_collView reloadData];
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }
                    else
                    {
                        [CommonFunctions alertTitle:@"" withMessage:[NSString stringWithFormat:@"20/%lu Uploaded",(unsigned long)imageArr.count]];
                        return;
                    }
                } else {
                    DLog(@"UIImagePickerControllerReferenceURL = %@", dict);
                }
            }  else {
                DLog(@"Uknown asset type");
            }
        }
        
        
        
        
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Image upload limit is upto 20."];
        
    }
    DLog(@"-----%d",imageArr.count);
    [_collView reloadData];
    [scrllVw setScrollEnabled:YES];
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [scrllVw setScrollEnabled:YES];
}



#pragma mark- UIPickerView Delegate Methods

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return propertyTypeArr.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return NSLocalizedString([[[propertyTypeArr objectAtIndex:row] objectForKey:@"Propertytype"]objectForKey:@"title"], nil);
    
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    txtPropertyType.text=[[[propertyTypeArr objectAtIndex:row] objectForKey:@"Propertytype"]objectForKey:@"title"];
    propertyTypeID=[[[propertyTypeArr objectAtIndex:row] objectForKey:@"Propertytype"]objectForKey:@"id"];
}

#pragma mark- UITableView Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return LocationArr.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30.0f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifair=@"MenuCustemCell";
    
    
    LocationCustomCell *cell=[tblVw dequeueReusableCellWithIdentifier:cellIdentifair];
    
    if(cell==nil)
    {
        cell=[[LocationCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifair];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    cell.lblName.text=LOCALIZATION([[LocationArr objectAtIndex:indexPath.row]objectForKey:@"locationName"]);
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    enterLocation.text=[[LocationArr objectAtIndex:indexPath.row] objectForKey:@"locationName"] ;
    locationID=[[LocationArr objectAtIndex:indexPath.row] objectForKey:@"locationID"];

    //userlet=[[[LocationArr objectAtIndex:indexPath.row] objectForKey:@"location"] objectForKey:@"lat"];
   // userlong=[[[LocationArr objectAtIndex:indexPath.row] objectForKey:@"location"] objectForKey:@"lng"];
    [tblVw setHidden:YES];
    tapGesture.enabled=YES;
}

#pragma mark- WeserviceAPI

-(void)getPropertyType
{
    //    NSString *useLatitude=[NSString stringWithFormat:@"%f",CurrentLatitude];
    //    NSString *userLongitude=[NSString stringWithFormat:@"%f",CurrentLongitude];
    //http://mymeetingdesk.com/mobile/baiti/mobile/propertytypes
    NSString *siteURL = @"propertytypes";
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN], @"token",language_id,@"language_id",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            
            propertyTypeArr=[responseDict objectForKey:@"data"];
            
    
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection or try again later" withDelegate:self withTag:111];
                                          }
                                          
                                      }];
}

-(void)getLocation:(NSString *)str
{
    DLog(@"-------String : %@",str);
    //http://mymeetingdesk.com/mobile/baiti/mobile/locations
    NSString *siteURL = @"locations";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:str, @"pressed_keyword",language_id,@"language_id",nil];
    
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        [activityIndicator stopAnimating];
        [activityIndicator setHidden:YES];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            
            DLog(@"--------- : %@",LocationArr);
            
        }
        else
        {
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                               
                                          }
                                          
                                      }];
}
- (IBAction)fullScreenCrossButton:(id)sender {
    [fullScreenView removeFromSuperview];
    [self fadeInAnimation:self.view];
}
@end
