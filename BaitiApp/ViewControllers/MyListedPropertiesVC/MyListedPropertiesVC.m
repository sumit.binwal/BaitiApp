//
//  MyListedPropertiesVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 01/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "MyListedPropertiesVC.h"
#import "DetailVC.h"
#import "PostProperty.h"


@interface MyListedPropertiesVC ()
{
    NSMutableArray *propertyListedArr;
    int totalPage;
    int apiCurrentpage;
    int totalRecord;
    int currentPage;
    NSString *propertyID;
    NSString *propertyOwnerID;
    NSString *propertyType;
    IBOutlet UILabel *lblMessage;
}
@end

@implementation MyListedPropertiesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.myListedPropertyTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    propertyListedArr =[[NSMutableArray alloc]init];
    _arrayForPrices=[[NSMutableArray alloc]initWithObjects:@"$ 45,0000",@"$ 300000",@"$ 500000",@"$ 2400000",@"$ 8094595" ,nil];
    _arrayForNumOfRooms=[[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6", nil];
    // Do any additional setup after loading the view from its nib.
    propertyType=@"1";
    totalRecord=1;
    currentPage =1;
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
    }
    else
    {
        isarabic=NO;
    }
    
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self myListedProperty];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
        
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setnavigationBar];
    [self setTitle];
    
}
#pragma mark-
#pragma mark-setNavigationbar

- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
    }

    titleView.text =LOCALIZATION(@"My Listed Properties") ;
    self.navigationItem.titleView = titleView;
    
    
    [titleView sizeToFit];
}
-(void)setnavigationBar
{
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftButton addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"list_icon"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    UIEdgeInsets imageInsets;
    
    if([SYSTEM_VERSION floatValue]>=7.0f)
        imageInsets = UIEdgeInsetsMake(0.0f,-6.0f,0.0f, 00.0f);
    else
        imageInsets = UIEdgeInsetsMake(0.0f, -6.0f, 0.0f, 0.0f);
    leftButton.imageEdgeInsets= imageInsets;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    
}
#pragma mark -
#pragma mark - tableViewDelegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return propertyListedArr.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyListedPropertyCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MyPropertyListCell"];
    
    if (cell==nil) {
        cell = [[MyListedPropertyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyListedPropertyCell"];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.lblForPrice.text=[[[propertyListedArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"price"];
    cell.lblForNumOfBedRooms.text=[[[propertyListedArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"no_of_bedroom"];
    cell.labelForLocationName.text=[[[propertyListedArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"location"];
    
    NSString *strPropertyType=[[[propertyListedArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"property_type"];
    if (isarabic)
    {
        cell.lblBed.text=@"السرير";
    }
    
    cell.lblForTypeOfFlats.text=LOCALIZATION(strPropertyType) ;
    
    
    [cell.imageforBuilderName setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[propertyListedArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    cell.rightButtons = @[ [MGSwipeButton buttonWithTitle:nil icon:[UIImage imageNamed:@"delete_icon"] backgroundColor:[UIColor colorWithRed:239.0/255.0 green:2.0/255 blue:125.0/255.0 alpha:1.0] callback:^BOOL(MGSwipeTableCell *sender) {
        
        propertyID=[NSString stringWithFormat:@"%@",[[[propertyListedArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"id"]];
        
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self deletePropertyAPI];
        [propertyListedArr removeObject:[propertyListedArr objectAtIndex:indexPath.row]];
        
        return  YES;
    }],
                           
                           [MGSwipeButton buttonWithTitle:nil icon:[UIImage imageNamed:@"edit_but"] backgroundColor:[UIColor colorWithRed:239.0/255.0 green:2.0/255 blue:125.0/255.0 alpha:1.0] callback:^BOOL(MGSwipeTableCell *sender) {
                               
                               
                               propertyID=[NSString stringWithFormat:@"%@",[[[propertyListedArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"id"]];
                               propertyOwnerID=[NSString stringWithFormat:@"%@",[[[propertyListedArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"user_id"]];
                               if ([CommonFunctions reachabiltyCheck]) {
                                   [CommonFunctions showActivityIndicatorWithText:@""];
                                   [self editPropertyDetails];
                               }
                               else
                               {
                                    [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
                               }
                               
                               
                               return YES;
                               
                           }]];
    cell.rightSwipeSettings.transition = MGSwipeTransitionDrag;
    
    if ((indexPath.row == [propertyListedArr count]-1) && ([propertyListedArr count] != totalRecord))
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        
        [self myListedProperty];
    }
    
    return cell;
    
    
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO; //tableview must be editable or nothing will work...
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    propertyID=[NSString stringWithFormat:@"%@",[[[propertyListedArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"id"]];
    propertyOwnerID=[NSString stringWithFormat:@"%@",[[[propertyListedArr objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"user_id"]];
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getPropertyDetails];
    }
    else
    {
         [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
        
    }
    
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(MyListedPropertyCell* )cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CGFloat  height=73;
    return height;
}


#pragma mark - SegmentControl

- (IBAction)forSaleAndRent:(id)sender {
    
    if (_segmentCotrl.selectedSegmentIndex==0)
    {
        [propertyListedArr removeAllObjects];
        propertyType=@"1";
        totalRecord=1;
        currentPage =1;
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self myListedProperty];
        }
        
        else
        {
             [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
            
        }    }
    else if (_segmentCotrl.selectedSegmentIndex==1)
    {
        [propertyListedArr removeAllObjects];
        propertyType=@"0";
        totalRecord=1;
        currentPage =1;
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self myListedProperty];
        }
        else
        {
             [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
            
        }
    }
}


#pragma mark- WebService API

-(void)myListedProperty
{
    //    [lblNoRecord setHidden:YES];
    if(totalRecord==[propertyListedArr count])
    {
        if (apiCurrentpage==totalPage)
        {
            [CommonFunctions removeActivityIndicator];
            return;
        }
    }
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyType, @"property_mode",[NSString stringWithFormat:@"%d",currentPage],@"pagenumber",language_id,@"language_id",nil];
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN], @"token",nil];
    
    
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/myfavouritelist
    NSString *siteURL = @"mypropertylist";
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_fav_property"] forKey:UD_FAV_PROPERTY];
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_listed_property"] forKey:UD_LISTED_PROPERTY];
            [[NSUserDefaults standardUserDefaults]synchronize];
            apiCurrentpage=[[responseDict objectForKey:@"currentPage"]intValue];
            totalPage=[[responseDict objectForKey:@"totalPages"]intValue];
            totalRecord=[[responseDict objectForKey:@"totalRecord"]intValue];
            if ([[[responseDict objectForKey:@"totalRecord"] stringValue] isEqualToString:@"0"]) {
                lblMessage.text=[responseDict objectForKey:@"replyMsg"];
                [_myListedPropertyTableView reloadData];
            }
            else
            {
                lblMessage.text=@"";
                
                apiCurrentpage=[[responseDict objectForKey:@"currentPage"]intValue];
                totalPage=[[responseDict objectForKey:@"totalPages"]intValue];
                totalRecord=[[responseDict objectForKey:@"totalRecord"]intValue];
                
                if ([self isNotNull:[responseDict objectForKey:@"data"]])
                {
                    
                    NSMutableArray *tempArr = [[NSMutableArray alloc]init];
                    // totalPage = [[responseDict objectForKey:@"totalPages"] intValue];
                    
                    NSMutableArray *arr = [[responseDict objectForKey:@"data"] mutableCopy];
                    
                    for (int i = 0; i < [arr count]; i++)
                    {
                        [tempArr addObject:[arr objectAtIndex:i]];
                        
                    }
                    
                    
                    if (currentPage == 1)
                    {
                        [propertyListedArr removeAllObjects];
                        
                    }
                    
                    for (int i = 0; i < [tempArr count]; i++)
                    {
                        [propertyListedArr addObject:[tempArr objectAtIndex:i]];
                    }
                    
                    [tempArr removeAllObjects];
                    
                    tempArr = nil;
                    
                    
                    currentPage++;
                    
                    // isLoading = NO;
                    
                }
                
                
                [_myListedPropertyTableView reloadData];
            }
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                               
                                          }
                                          
                                      }];
}
-(void)getPropertyDetails
{
    
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/propertydetail
    NSString *siteURL = @"propertydetail";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyID, @"propertyid",propertyOwnerID,@"property_owner_id",language_id,@"language_id",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:[[[responseDict objectForKey:@"data"]objectForKey:@"Property"]objectForKey:@"isFav"]  forKey:UD_FAVORITE];
            [[NSUserDefaults standardUserDefaults]synchronize];
            DetailVC *dtvc=ICLocalizedViewController([DetailVC class]);//[[DetailVC alloc]initWithNibName:@"DetailVC" bundle:nil];
            dtvc.detailDataDict=[responseDict objectForKey:@"data"];
            [self.navigationController pushViewController:dtvc animated:YES];
        }
        else
        {
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];            }
            
            
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                               
                                          }
                                          
                                      }];
}

-(void)deletePropertyAPI
{
    
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/propertydetail
    NSString *siteURL = @"property_delete";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyID, @"property_id",language_id,@"language_id",nil];
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN], @"token",nil];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_fav_property"] forKey:UD_FAV_PROPERTY];
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_listed_property"] forKey:UD_LISTED_PROPERTY];
            [[NSUserDefaults standardUserDefaults]synchronize];
            if (propertyListedArr.count<1) {
                lblMessage.text=LOCALIZATION(@"No data found!!");
                [_myListedPropertyTableView reloadData];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_fav_property"] forKey:UD_FAV_PROPERTY];
                [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_listed_property"] forKey:UD_LISTED_PROPERTY];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [_myListedPropertyTableView reloadData];
            }
        }
        else
        {
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];            }
            
            
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                               
                                          }
                                          
                                      }];
}

-(void)editPropertyDetails
{
    
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/editproperty
    NSString *siteURL = @"editproperty";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyID, @"property_id",language_id,@"language_id",nil];
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN], @"token",nil];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            PostProperty *ppvc=ICLocalizedViewController([PostProperty class]);//[[PostProperty alloc]initWithNibName:@"PostProperty" bundle:nil];
            ppvc.detailDataDict=[responseDict objectForKey:@"data"];
            [self.navigationController pushViewController:ppvc animated:YES];
        }
        else
        {
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];            }
            
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                               
                                          }
                                          
                                      }];
}
@end
