//
//  MyListedPropertiesVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 01/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyListedPropertyCell.h"
#import "MGSwipeButton.h"


@interface MyListedPropertiesVC : UIViewController<UIGestureRecognizerDelegate,MGSwipeTableCellDelegate>
{
    BOOL isarabic;
}

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentCotrl;
@property (strong, nonatomic) IBOutlet UITableView *myListedPropertyTableView;

- (IBAction)forSaleAndRent:(id)sender;

@property NSMutableArray * arrayForPrices;
@property NSMutableArray * arrayForNumOfRooms;


@end
