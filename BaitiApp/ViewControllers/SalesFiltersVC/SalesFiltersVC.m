//
//  SalesFiltersVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "SalesFiltersVC.h"
#import "MARKRangeSlider.h"
#import "HomeVC.h"
#import "UIColor+Demo.h"
#import "SalesFilterCustomTableCell.h"
#import "DoubleSlider.h"

#define SLIDER_VIEW_TAG     1234

@interface SalesFiltersVC ()
{
    NSString *numberofBedrooms;
    NSMutableArray *mostVwLocationArr;
    IBOutlet UITableView *tblVw;
    IBOutlet UIView *customePIckerView;
    IBOutlet UIPickerView *pickerVw;
    IBOutlet UIView *toolbarVw;
    NSMutableArray *searchTimeArr;
    NSString *searchTimeArrIndex;
    
    NSString *minValue;
    NSString *maxValue;
    NSString *minBed;
    NSString *maxBed;
}
@property (nonatomic, strong) DoubleSlider *rangeSlider;
@property (nonatomic, strong) UILabel *label;
@end

@implementation SalesFiltersVC
@synthesize propertyID;
@synthesize dictaFilter;
@synthesize filterDelegate;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.txtFldForPicker setValue:[UIColor blackColor]forKeyPath:@"_placeholderLabel.textColor"];
    _txtFldForPicker.inputView=customePIckerView;
    _txtFldForPicker.inputAccessoryView=toolbarVw;
   // [self forSlider];
    [self setUpViewComponents];
    
    [self getMostViewedLocation];
    [self getSearchTime];
    DLog(@"----- %@",propertyID);
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
    }
    else
    {
        isarabic=NO;
    }
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapClicked)];
    tapGesture.delegate=self;
    tapGesture.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tapGesture];
    
    // Do any additional setup after loading the view from its nib.
    
    if (self.locationArr.count == 0) {
        [self.sliderViewVSConst setConstant:-48];
    }
    else{
        
        NSString *locStr = @"";
        
        for (int i = 0 ; i < self.locationArr.count; i++) {
            NSDictionary *locDict = [self.locationArr objectAtIndex:i];
            
            if(i == 0)
                locStr = [NSString stringWithFormat:@"%@",[locDict valueForKey:@"address"]];
            else
                locStr = [NSString stringWithFormat:@"%@, %@",locStr,[locDict valueForKey:@"address"]];
        }
        
        self.lblLocation.text = locStr;
        
    }
    
}
-(void)singleTapClicked
{
    [self.view endEditing:YES];
}
-(void)viewWillLayoutSubviews
{
    self.rangeSlider.frame = CGRectMake(0, 10, self.sliderView.frame.size.width,10);
}
-(void)viewWillAppear:(BOOL)animated
{
    bedroomSelected=NO;
    propertySelected=NO;
    
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setnavigationBar];
    [self setTitle];
    if (!dictaFilter.count==0)
    {
        [self setFilterPreviousValues];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - set NavigationBar
- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        self.navigationItem.titleView = titleView;
        
    }

        if ([propertyID isEqualToString:@"1"])
        {
            titleView.text =LOCALIZATION(@"Sales Filters") ;
        }
        else
        {
            titleView.text =LOCALIZATION(@"Rent Filters") ;
        }
    [titleView sizeToFit];
}
-(void)setnavigationBar
{
   [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 28, 28)];
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
    UIEdgeInsets imageInsets;
    
    if([SYSTEM_VERSION floatValue]>=7.0f)
        imageInsets = UIEdgeInsetsMake(0.0f,-7.0f,0.0f, 00.0f);
    else
        imageInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    leftButton.imageEdgeInsets= imageInsets;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    
    UIButton * rightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 60, 22)];
    [rightButton addTarget:self action:@selector(goToApply:) forControlEvents:UIControlEventTouchUpInside];

         [rightButton setTitle:LOCALIZATION(@"Apply")  forState:UIControlStateNormal];
    
   
    rightButton.titleLabel.font=[UIFont fontWithName:@"OXYGEN-REGULAR" size:12];
    [rightButton setBackgroundImage:[UIImage imageNamed:@"search_filter_applybut"] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:rightButton];
    
    
}
-(void)goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)goToApply:(id)sender
{
//    if (bedroomSelected && propertySelected)
//    {
        NSMutableDictionary *locationDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Kuwait",@"address",@"29.31166",@"lat",@"47.481766",@"long", nil];
        NSMutableArray *locationArr=[[NSMutableArray alloc]initWithObjects:locationDict, nil];
        
        if (minBed.length<1) {
            minBed=[dictaFilter valueForKey:@"minbeds"];
            maxBed=[dictaFilter valueForKey:@"maxbeds"];
            
        }
        
        
        NSMutableDictionary *params;
        params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:minBed,@"minbeds",maxBed,@"maxbeds",minValue,@"minprice",maxValue,@"maxprice",@"1",@"adfilter",@"1",@"search",searchTimeArrIndex,@"created",propertyID,@"property_mode",locationArr,@"locations",nil];
        [filterDataArr addObject:params];
        
        
        if([filterDelegate respondsToSelector:@selector(setfilteredResults:)])
        {
            [filterDelegate setfilteredResults:params];
        }
        
        [self.navigationController popViewControllerAnimated:YES];
//    }
//    else
//    {
//        if (isarabic)
//        {
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"بيتي التطبيق" message:@"إرضاء أرقام مختارة من غرف النوم أولا واليوم شارك!" delegate:nil cancelButtonTitle:@"حسنا" otherButtonTitles:nil, nil];
//            [alert show];
//
//        }
//        else
//        {
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"BaitiApp" message:@"Please selected numbers of bedrooms First and posted day !" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//            [alert show];
//
//        }
// }
}

#pragma mark- set filter Values
-(int)getRangeSliderValue:(int)value1{
    if (value1==1000) {
        return 1;
    }
    else if (value1==5000)
    {
        return 2;
    }
    else if (value1==10000)
    {
        return 3;
    }
    else if (value1==50000)
    {
        return 4;
    }
    else if (value1==100000)
    {
        return 5;
    }
    else if (value1==200000)
    {
        return 6;
    }
    return 0;
    
}
-(void)setFilterPreviousValues
{
    DLog(@"%@",dictaFilter);
    
    float minbvalue=[[dictaFilter valueForKey:@"minprice"] floatValue];
    float maxbValue=[[dictaFilter valueForKey:@"maxprice"] floatValue];
    
    self.rangeSlider.minSelectedValue=[self getRangeSliderValue:minbvalue];

    self.rangeSlider.maxSelectedValue=[self getRangeSliderValue:maxbValue];
    
    rangeLabel.text=[NSString stringWithFormat:@"%0.0f KWD - %0.0f KWD",minbvalue,maxbValue];
    
    
   NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *myString = [prefs stringForKey:@"date"];
    
    self.txtFldForPicker.text=myString;
    searchTimeArrIndex=[prefs stringForKey:@"index"];
    
    NSString *strBedNumbers=[dictaFilter valueForKey:@"maxbeds"];
    
    if ([strBedNumbers isEqualToString:@"1"])
    {
        [_fristBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_fristBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
     


    }
    else if ([strBedNumbers isEqualToString:@"2"])
    {
        [_fristBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_fristBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_secondBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_secondBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else if ([strBedNumbers isEqualToString:@"3"])
    {
        [_fristBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_fristBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_secondBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_secondBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_thirdBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_thirdBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else if ([strBedNumbers isEqualToString:@"4"])
    {
        [_fristBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_fristBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_secondBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_secondBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_thirdBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_thirdBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_fourthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_fourthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else if ([strBedNumbers isEqualToString:@"5"])
    {
        [_fristBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_fristBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_secondBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_secondBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_thirdBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_thirdBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_fourthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_fourthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_fifthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_fifthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    }    else
    {
        [_fristBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_fristBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_secondBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_secondBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_thirdBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_thirdBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_fourthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_fourthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_fifthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_fifthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_sixthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
        [_sixthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];


    }
    
    bedroomSelected=YES;
    
    if (dictaFilter.count>6)
    {
        propertySelected=YES;
    }
    else
    {
        propertySelected=NO;
    }
    
}



#pragma mark- UIPickerView Delegate Methods

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return searchTimeArr.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [searchTimeArr objectAtIndex:row] ;
    
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _txtFldForPicker.text=[searchTimeArr objectAtIndex:row];
    searchTimeArrIndex=[NSString stringWithFormat:@"%ld",(long)row];
}

- (IBAction)barCancelButtonClicked:(id)sender {
    propertySelected=NO;
    [self.view endEditing:YES];
    //[scrllVw setContentOffset:CGPointZero];
    _txtFldForPicker.text=@"";
}

- (IBAction)doneButtonClicked:(id)sender {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    propertySelected=YES;
    
        if(_txtFldForPicker.text.length<1)
        {
            _txtFldForPicker.text=[searchTimeArr objectAtIndex:0];
            [prefs setObject:_txtFldForPicker.text forKey:@"date"];
        
            searchTimeArrIndex=@"0";
        }
    
    else
    {
         [prefs setObject:_txtFldForPicker.text forKey:@"date"];
       // [scrllVw setContentOffset:CGPointZero];
        
    }
    
     [prefs setObject:searchTimeArrIndex forKey:@"index"];
    
    [prefs synchronize];
                [_txtFldForPicker resignFirstResponder];
}

#pragma mark - tableViewDelegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [mostVwLocationArr count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifair=@"cellIdentifier";
    
    SalesFilterCustomTableCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifair];
    
    if(cell==nil)
    {
        cell=[[SalesFilterCustomTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifair];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.lblLocationName.text=NSLocalizedString([CommonFunctions trimSpaceInString:[[[mostVwLocationArr objectAtIndex:indexPath.row]objectForKey:@"MvLocation"]objectForKey:@"location"]], nil);//[CommonFunctions trimSpaceInString:[[[mostVwLocationArr objectAtIndex:indexPath.row]objectForKey:@"MvLocation"]objectForKey:@"location"]];
    
    if ([cell.lblLocationName.text rangeOfString:@"U06"].location == NSNotFound) {
     
    }
    else
    {
           [cell.lblLocationName setTextAlignment:NSTextAlignmentRight];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell* )cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
#pragma mark-slider

#pragma mark - Actions

- (void)rangeSliderValueDidChange:(MARKRangeSlider *)slider
{
    [self.view endEditing:YES];
    
}

#pragma mark - UI

- (void)setUpViewComponents
{


    // Init slider

//    self.rangeSlider = [[MARKRangeSlider alloc] initWithFrame:CGRectZero];
//    [self.rangeSlider addTarget:self
//                         action:@selector(rangeSliderValueDidChange:)
//               forControlEvents:UIControlEventValueChanged];
    
    
     //for testing purposes only
    
    if ([propertyID isEqualToString:@"1"]) {
        
        self.sliderView.minimumValue = 1;
        self.sliderView.maximumValue = 23;
        
        self.sliderView.lowerValue = 1;
        self.sliderView.upperValue = 23;
        
        self.sliderView.minimumRange = 3;
        
        
        
        [self SaleSliderValueChanged];
        
    }
    else{
        
        self.sliderView.minimumValue = 1;
        self.sliderView.maximumValue = 34;
        
        self.sliderView.lowerValue = 1;
        self.sliderView.upperValue = 34;
        
        self.sliderView.minimumRange = 5;
        
        [self RentSliderValueChanged];
        
    }


}
- (IBAction)labelSliderChanged:(NMRangeSlider*)sender
{
    if ([propertyID isEqualToString:@"1"])
    {
        
        [self SaleSliderValueChanged];
        
    }
    else
    {
        [self RentSliderValueChanged];
        
    }
}

-(void)RentSliderValueChanged{

    float FirstValue=0;
    float SecondValue=0;
    
    FirstValue=(roundf(self.sliderView.lowerValue))*100;
    SecondValue=(roundf(self.sliderView.upperValue) - 4)*100;
    
    rangeLabel.text=[NSString stringWithFormat:@"%0.0f KWD to %0.0f KWD",FirstValue,SecondValue];
    minValue=[NSString stringWithFormat:@"%0.0f",FirstValue];
    maxValue=[NSString stringWithFormat:@"%0.0f",SecondValue];
    
}
-(void)SaleSliderValueChanged{

    float FirstValue=0;
    float SecondValue=0;
    
    FirstValue=(roundf(self.sliderView.lowerValue)*2 - 1)*25000;
    
    if (roundf(self.sliderView.upperValue) == 23) {
        SecondValue=(roundf(self.sliderView.upperValue)*2 - 6)*25000;
    }
    else{
        SecondValue=(roundf(self.sliderView.upperValue)*2 - 5)*25000;
    }
    
    rangeLabel.text=[NSString stringWithFormat:@"%0.0f KWD to %0.0f KWD",FirstValue,SecondValue];
    minValue=[NSString stringWithFormat:@"%0.0f",FirstValue];
    maxValue=[NSString stringWithFormat:@"%0.0f",SecondValue];
    
}
/*
-(void)ChangeValueSliderRangeSale
{
    float FirstValue=0;
    float SecondValue=0;
    NSLog(@"%f",self.rangeSlider.leftValue);
    if (roundf(self.rangeSlider.leftValue)==1) {
        FirstValue=roundf(self.rangeSlider.leftValue)*25000;
    }
    else if (roundf(self.rangeSlider.leftValue)==2)
    {
     FirstValue=3*25000;
    }
    else if (roundf(self.rangeSlider.leftValue)==3)
    {
      FirstValue=5*25000;
    }
    else if (roundf(self.rangeSlider.leftValue)==4)
    {
      FirstValue=7*25000;
    }
    else if (roundf(self.rangeSlider.leftValue)==5)
    {
      FirstValue=9*25000;
    }
    
    if (roundf(self.rangeSlider.rightValue)==6) {
        SecondValue=roundf(self.rangeSlider.rightValue)*30000+20000;
    }
    else if (roundf(self.rangeSlider.rightValue)==2)
    {
       SecondValue=roundf(self.rangeSlider.rightValue)*2500;
    }
    else if (roundf(self.rangeSlider.rightValue)==3)
    {
        SecondValue=roundf(self.rangeSlider.rightValue)*3000+1000;
    }
    else if (roundf(self.rangeSlider.rightValue)==4)
    {
       SecondValue=roundf(self.rangeSlider.rightValue)*10000+10000;
    }
    else if (roundf(self.rangeSlider.rightValue)==5)
    {
       SecondValue=roundf(self.rangeSlider.rightValue)*20000;
    }

     rangeLabel.text=[NSString stringWithFormat:@"%0.0f KWD to %0.0f KWD",FirstValue,SecondValue];
    minValue=[NSString stringWithFormat:@"%0.0f",FirstValue];
    maxValue=[NSString stringWithFormat:@"%0.0f",SecondValue];
}
*/


//-(void)forSlider
//{
//    //DoubleSlider setup
//    DoubleSlider *rangeSlider;
//    if ([propertyID isEqualToString:@"1"]) {
//    rangeSlider = [DoubleSlider doubleSlider:50000 Value:2000000.0];
//    }
//    else
//    {
//    rangeSlider = [DoubleSlider doubleSlider:200 Value:5000];
//    }
//
//    [rangeSlider addTarget:self action:@selector(valueChangedForDoubleSlider:) forControlEvents:UIControlEventValueChanged];
//  //  slider.center = self.view.center;
////    slider.tag = SLIDER_VIEW_TAG; //for testing purposes only
//    [self.view addSubview:rangeSlider];
//    
//    
//    //get the initial values
//    //slider.transform = CGAffineTransformRotate(slider.transform, 90.0/180*M_PI);      //make it vertical
//    
//    //dynamically set the slider positions
//    //[slider moveSlidersToPosition:[NSNumber numberWithInt:300] :[NSNumber numberWithInt:77] animated:NO];
//    
//    [self valueChangedForDoubleSlider:rangeSlider];
//
//}
//
//#pragma mark Control Event Handlers
//
//- (void)valueChangedForDoubleSlider:(DoubleSlider *)rangeSlider
//{

//}
//
//
//
//- (void)devButtonHandler:(id)sender {
//    DoubleSlider *rangeSlider = (DoubleSlider *)[self.view viewWithTag:SLIDER_VIEW_TAG];
//    if (rangeSlider) {
//        [rangeSlider moveSlidersToPosition:[NSNumber numberWithInt:arc4random() % 50] :[NSNumber numberWithInt:51 + arc4random() % 50] animated:YES];
//    }
//}



#pragma mark-For bedroom selection action
- (IBAction)firstBtnPressed:(id)sender {

    [self.view endEditing:YES];
     bedroomSelected=YES;
    
    UIButton *btn=(UIButton *)sender;
    int btntag=(int)btn.tag;
        [_fristBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number"] forState:UIControlStateNormal];
        [_secondBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number"] forState:UIControlStateNormal];
        [_thirdBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number"] forState:UIControlStateNormal];
        [_fourthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number"] forState:UIControlStateNormal];
        [_fifthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number"] forState:UIControlStateNormal];
        [_sixthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number"] forState:UIControlStateNormal];
        [_fristBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_secondBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_thirdBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_fourthBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_fifthBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_sixthBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    switch (btntag) {
            
           
            
        case 1:
            
            [_fristBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_fristBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

            minBed=@"1";
maxBed=@"1";
            break;
        case 2:
            [_fristBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_fristBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_secondBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_secondBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            minBed=@"1";
            maxBed=@"2";
            break;
        case 3:
            [_fristBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_fristBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_secondBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_secondBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_thirdBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_thirdBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

            
            minBed=@"1";
            maxBed=@"3";
            break;
        case 4:
            [_fristBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_fristBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_secondBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_secondBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_thirdBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_thirdBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_fourthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_fourthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

            minBed=@"1";
            maxBed=@"4";
            break;
        case 5:
            [_fristBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_fristBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_secondBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_secondBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_thirdBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_thirdBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_fourthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_fourthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_fifthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_fifthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

            minBed=@"1";
            maxBed=@"5";
            break;
            
        case 6:
            
            [_fristBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_fristBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_secondBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_secondBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_thirdBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_thirdBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_fourthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_fourthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_fifthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_fifthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_sixthBtn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [_sixthBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

            minBed=@"1";
            maxBed=@"40";
            break;
    }
}

#pragma mark- Web Service API

-(void)getSearchTime
{
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/searchtime
    NSString *siteURL = @"searchtime";
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:language_id,@"language_id",nil];
    
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];

            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            searchTimeArr=[responseDict objectForKey:@"data"];
//            DLog(@"%@",mostVwLocationArr);
//            [tblVw reloadData];
        }
        else
        {
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                        [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }

                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                   [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")]; 
                                          }
                                          
                                      }];
}


-(void)getMostViewedLocation
{
   
    //http://mymeetingdesk.com/mobile/baiti/mobile/mostviewed
    NSString *siteURL = @"mostviewed";

    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];

            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            mostVwLocationArr=[responseDict objectForKey:@"data"];
            DLog(@"%@",mostVwLocationArr);
            [tblVw reloadData];
        }
        else
        {
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
            
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                        [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }

                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                   [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")]; 
                                          }
                                          
                                      }];
}


@end
