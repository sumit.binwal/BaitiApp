//
//  MyProfileVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 25/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "MyProfileVC.h"
#import "ChangePassword.h"
#import "EditProfileVC.h"

@interface MyProfileVC ()
{
    IBOutlet UILabel *lblAddress;
    
    IBOutlet UILabel *lblPhoneNumber;
    IBOutlet UILabel *lblEmail;
    IBOutlet UILabel *lblPropertyListed;
    IBOutlet UILabel *lblFavortiesCount;
    IBOutlet UILabel *lblName;
}


@end

@implementation MyProfileVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _userProfilePic.layer.cornerRadius = _userProfilePic.frame.size.width / 2;
    _userProfilePic.clipsToBounds = YES;
    
}



-(void)viewWillAppear:(BOOL)animated
{
    NSData *dataDict=[[NSUserDefaults standardUserDefaults]objectForKey:UD_USERINFO];
    NSDictionary *dict=[NSKeyedUnarchiver unarchiveObjectWithData:dataDict];
    
    lblAddress.text=[dict objectForKey:@"address"];
    lblEmail.text=[dict objectForKey:@"email"];
    lblFavortiesCount.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_FAV_PROPERTY]];
    
    
    NSData *data = [[dict objectForKey:@"username"]  dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSString *valueEmoj = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    lblName.text=valueEmoj;
    if ([self isNotNull:[dict objectForKey:@"phone"]]) {
        lblPhoneNumber.text=[dict objectForKey:@"phone"];
    }
    else
    {
        lblPhoneNumber.text=@"";
    }
    
    lblPropertyListed.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_LISTED_PROPERTY]];
    [_userProfilePic setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict objectForKey:@"image"]]] completed:nil usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    
    
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setTitle];
    [self setnavigationBar];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - set NavigationBar

- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        
    }

        titleView.text =LOCALIZATION(@"My Profile") ;
    
    
    
    [titleView sizeToFit];
}
-(void)setnavigationBar
{
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 28, 28)];
    UIEdgeInsets imageInsets;
    
    if([SYSTEM_VERSION floatValue]>=7.0f)
        imageInsets = UIEdgeInsetsMake(0.0f,-6.0f,0.0f, 00.0f);
    else
        imageInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    leftButton.imageEdgeInsets= imageInsets;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [leftButton addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
}


- (IBAction)settingsBtnAction:(id)sender
{
    
    ChangePassword * changePwd=ICLocalizedViewController([ChangePassword class]);//[[ChangePassword alloc]initWithNibName:@"ChangePassword" bundle:nil];
    [self.navigationController pushViewController:changePwd animated:YES];
}

- (IBAction)goToEditVC:(id)sender
{
    
    EditProfileVC * edit=ICLocalizedViewController([EditProfileVC class]);//[[EditProfileVC alloc]initWithNibName:@"EditProfileVC" bundle:nil];
    [self.navigationController pushViewController:edit animated:YES];
    
}

@end
