//
//  ViewController.h
//  BaitiApp
//
//  Created by Shweta Rao on 24/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignInVC : UIViewController<UIScrollViewDelegate>
{
     NSMutableDictionary *msgDict;
    BOOL isarabic;
    IBOutlet UIImageView *imgSignVc;
}

@property (strong, nonatomic) IBOutlet UITextField *txtFldForEnterEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtFldForEnterPasswd;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)SignInAction:(id)sender;
- (IBAction)forgotPasswordAction:(id)sender;
- (IBAction)signUpAction:(id)sender;

@property (strong,nonatomic)  NSMutableDictionary *msgDict;



@end

