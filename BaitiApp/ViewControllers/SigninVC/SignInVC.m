//
//  SignInVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 24/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "SignInVC.h"
#import "ForgotPasswordVC.h"
#import "SignUpVC.h"
#import "HomeVC.h"
#import "IIViewDeckController.h"

@interface SignInVC ()<UIAlertViewDelegate>
{
    
    IBOutlet UIButton *signInBtn;
}
@end

@implementation SignInVC
@synthesize txtFldForEnterEmail,txtFldForEnterPasswd,scrollView,msgDict;

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
    [[UIApplication sharedApplication]setStatusBarHidden:YES];
    [self setButtonText];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    msgDict=[[NSMutableDictionary alloc]init];
    [scrollView setScrollEnabled:NO];
    
    [txtFldForEnterEmail setValue:[UIColor colorWithRed:74.0f/255.0f green:74.0f/255.0f blue:74.0f/255.0f alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    [txtFldForEnterPasswd setValue:[UIColor colorWithRed:74.0f/255.0f green:74.0f/255.0f blue:74.0f/255.0f alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
        imgSignVc.image=[UIImage imageNamed:@"Signiv"];
    }
    else
    {
        isarabic=NO;
    }

    
// Do any additional setup after loading the view from its nib.
}

-(void)setButtonText
{
    NSString *string0;
    NSString *string1;
    NSString *text;

        string0 =@"Sign" ;
        string1=@"In" ;
        
        text = [NSString stringWithFormat:@"%@ %@",string0,string1];
        
    
    
    
    //whole String attribute
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor whiteColor],
                              NSFontAttributeName:[UIFont systemFontOfSize:14]
                              };
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(text) attributes:attribs];
    
    NSRange string0Range = [text rangeOfString:string0];
    NSRange string1Range = [text rangeOfString:string1];
    
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Bold" size:18.0f]}  range:string0Range];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Light" size:18.0f]}  range:string1Range];
    
    [signInBtn setAttributedTitle:attributedText forState:UIControlStateNormal];
    
}

- (IBAction)SignInAction:(id)sender {
    
    [self.view endEditing:YES];
    if([self validate])
    {
        if([CommonFunctions reachabiltyCheck])
        {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self UserLoginApi];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
        }

    }
    
}
- (IBAction)forgotPasswordAction:(id)sender
{
   ForgotPasswordVC *ForgotVC  = ICLocalizedViewController([ForgotPasswordVC class]);
    [self presentViewController:ForgotVC animated:YES completion:nil];
    
}
- (IBAction)signUpAction:(id)sender
{
    
    SignUpVC *signUpVC  = ICLocalizedViewController([SignUpVC class]);
    [self presentViewController:signUpVC animated:YES completion:nil];

}


#pragma mark - TextFiled Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == txtFldForEnterEmail) {
        [textField resignFirstResponder];
        [txtFldForEnterPasswd becomeFirstResponder];
    }
    if (textField==txtFldForEnterPasswd) {
        [textField resignFirstResponder];
    }
    return YES;
}

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    [self scrollViewToCenterOfScreen:textField];
//    return YES;
//}
//-(BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    switch (textField.tag)
//    {
//        case 1:
//            [txtFldForEnterPasswd becomeFirstResponder];
//            break;
//        case 2:
//            txtFldForEnterPasswd.text=textField.text;
//             [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
//            [self.view endEditing:YES];
//            break;
//            
//    }
//    return YES;
//}
-(BOOL)validate
{
    BOOL isValidate = YES;
    
    if([Validate isNull:txtFldForEnterEmail.text])
    {
        isValidate=NO;

            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Baiti")  message:LOCALIZATION(@"Please enter email id")  delegate:self cancelButtonTitle:LOCALIZATION(@"OK")  otherButtonTitles:nil];
            [alert show];
       
    }
    
    else if (![Validate isValidEmailId:txtFldForEnterEmail.text])
    {
        isValidate=NO;

            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Baiti")  message:LOCALIZATION(@"Please enter valid email")  delegate:self cancelButtonTitle:LOCALIZATION(@"OK")  otherButtonTitles:nil];
            [alert show];
    }
    
    else if([Validate isNull:txtFldForEnterPasswd.text])
    {
        isValidate = NO;

            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Baiti")  message:LOCALIZATION(@"Please enter email id")  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
    }
    return isValidate ;
}


-(void)UserLoginApi
{
    NSString *useLatitude=[NSString stringWithFormat:@"%f",CurrentLatitude];
    NSString *userLongitude=[NSString stringWithFormat:@"%f",CurrentLongitude];
    //http://192.168.0.170/baiti/mobile/login
    NSString *siteURL = @"login";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[CommonFunctions trimSpaceInString:txtFldForEnterEmail.text], @"email",[CommonFunctions trimSpaceInString:txtFldForEnterPasswd.text], @"password",useLatitude, @"mlat",userLongitude, @"mlong",language_id,@"language_id",userDeviceToken, @"deviceToken",nil];
    
    NSLog(@"%@",params);
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];

    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        NSLog(@"responce dict %@",responseDict);
        [CommonFunctions removeActivityIndicator];
if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];


        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            
            
            if ([[UIDevice currentDevice].systemVersion floatValue]>8.0) {
                UIUserNotificationType types = UIUserNotificationTypeSound|UIUserNotificationTypeBadge | UIUserNotificationTypeAlert;
                UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
                [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
            } else {
                [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
            }

            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:responseDict];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:UD_USERINFO];
            
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_fav_property"] forKey:UD_FAV_PROPERTY];
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"my_listed_property"] forKey:UD_LISTED_PROPERTY];
            [[NSUserDefaults standardUserDefaults]setObject:[responseDict objectForKey:@"token"] forKey:UD_TOKEN];

            [[NSUserDefaults standardUserDefaults]synchronize];
            
            HomeVC *resourceVC  = ICLocalizedViewController([HomeVC class]);
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:resourceVC animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:resourceVC]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
            if (isarabic)
            {
               [CommonFunctions alertTitleArabic:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil) withDelegate:self withTag:200];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"] withDelegate:self withTag:200];
            }

            
        }
        else
        {
            
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }

            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                        [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }

                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                   [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")]; 
                                          }
                                          
                                      }];
}

-(void)showAlert: (NSDictionary *)alertData
{
    NSString *strmessage=[alertData valueForKey:@"message"];

        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Email cannot be empty") message:strmessage delegate:self cancelButtonTitle:LOCALIZATION(@"OK")  otherButtonTitles:nil];
        [alert show];
}

-(void)OnSuccessMessage:(NSDictionary*)alertData
{
   
//    HomeVC *homeVC=[[HomeVC alloc]initWithNibName:@"HomeVC" bundle:nil];
//    [self presentViewController:homeVC animated:YES completion:nil];
//
//     self.navigationController.navigationBarHidden=NO;
//    UINavigationController * nav=[[UINavigationController alloc]initWithRootViewController:homeVC];
//    [self.navigationController pushViewController:nav animated:YES];

    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==200)
    {
        IIViewDeckController *controller=[APPDELEGATE generateControllerStack];
        APPDELEGATE.window.rootViewController=controller;
        
    }
}
- (IBAction)skipButtonClicked:(id)sender {
    IIViewDeckController *controller=[APPDELEGATE generateControllerStack];
    APPDELEGATE.window.rootViewController=controller;
}


@end
